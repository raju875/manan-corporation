<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/manan-administration2018','AdminMaster@index');
Route::get('/manan-administration2018/dashboard','AdminMaster@index');

//About profile
Route::get('/manan-administration2018/profile','ProfileController@profile');
Route::post('/manan-administration2018/update-profile','profileController@updateProfile');

//About Logo
Route::get('/manan-administration2018/logo/logo-add-form','LogoController@logoAddForm');
Route::post('/manan-administration2018/logo/add-new-logo','LogoController@addNewLogo');
Route::get('/manan-administration2018/logo/logo-manage','LogoController@manageLogo');
Route::get('/manan-administration2018/logo/unpublished-logo/{id}','LogoController@unpublishedLogo');
Route::get('/manan-administration2018/logo/published-logo/{id}','LogoController@publishedLogo');
Route::get('/manan-administration2018/logo/editable-logo-form/{id}','LogoController@editableLogoForm');
Route::post('/manan-administration2018/logo/update-logo','LogoController@updateLogo');
Route::get('/manan-administration2018/logo/delete-logo/{id}','LogoController@deleteLogo');
Route::get('/manan-administration2018/logo/logo-view/{id}','LogoController@viewLogo');

//About Banner
Route::get('/manan-administration2018/banner/banner-add-form','BannerController@bannerAddForm');
Route::post('/manan-administration2018/banner/add-new-banner','BannerController@addNewBanner');
Route::get('/manan-administration2018/banner/banner-manage','BannerController@bannerManage');
Route::get('/manan-administration2018/banner/unpublished-banner/{id}','BannerController@unpublishedBanner');
Route::get('/manan-administration2018/banner/published-banner/{id}','BannerController@publishedBanner');
Route::get('/manan-administration2018/banner/editable-banner-form/{id}','BannerController@editableBannerForm');
Route::post('/manan-administration2018/banner/update-banner','BannerController@updateBannerInfo');
Route::get('/manan-administration2018/banner/delete-banner/{id}','BannerController@deleteBannerInfo');
Route::get('/manan-administration2018/banner/banner-view/{id}','BannerController@BannerView');

//About AboutUs
Route::get('/manan-administration2018/about/about-form','AboutController@aboutForm');
Route::post('/manan-administration2018/about/add-new-about','AboutController@addNewAbout');
Route::get('/manan-administration2018/about/manage-about','AboutController@manageAbout');
Route::get('/manan-administration2018/about/unpublished-about/{id}','AboutController@unpublishedAbout');
Route::get('/manan-administration2018/about/published-about/{id}','AboutController@publishedAbout');
Route::get('/manan-administration2018/about/editable-about-form/{id}','AboutController@editableAboutForm');
Route::post('/manan-administration2018/about/update-about','AboutController@updateAbout');
Route::get('/manan-administration2018/about/delete-about/{id}','AboutController@deleteAbout');
Route::get('/manan-administration2018/about/view-about/{id}','AboutController@viewAbout');

//About Project Category
Route::get('/manan-administration2018/project-category/category-project-form','ProjectCategoryController@categoryProjectForm');
Route::post('/manan-administration2018/project-category/save-category-project-info','ProjectCategoryController@saveCategoryProjectInfo');
Route::get('/manan-administration2018/project-category/manage-category-project','ProjectCategoryController@manageCategoryProjectInfo');
Route::get('/manan-administration2018/project-category/unpublished-category-project/{id}','ProjectCategoryController@unpublishedCategoryProjectInfo');
Route::get('/manan-administration2018/project-category/published-category-project/{id}','ProjectCategoryController@publishedCategoryProjectInfo');
Route::get('/manan-administration2018/project-category/editable-category-project-form/{id}','ProjectCategoryController@editableCategoryProjectform');
Route::post('/manan-administration2018/project-category/update-category-project','ProjectCategoryController@updateCategoryProjectInfo');
Route::get('/manan-administration2018/project-category/delete-category-project-info/{id}','ProjectCategoryController@deleteCategoryProjectInfo');
Route::get('/manan-administration2018/project-category/view-project-category/{id}','ProjectCategoryController@projectCategoryDetails');

//About Error
Route::get('/manan-administration2018/error/error-500/{aleart}','ErrorController@error500');


//About Project
Route::get('/manan-administration2018/project/project-form','ProjectController@projectForm');
Route::post('/manan-administration2018/project/create-project','ProjectController@createProject');
Route::get('/manan-administration2018/project/manage-project','ProjectController@manageProject');
Route::get('/manan-administration2018/project/unpublished-project/{id}','ProjectController@unpublishedProject');
Route::get('/manan-administration2018/project/published-project/{id}','ProjectController@publishedProject');
Route::get('/manan-administration2018/project/editable-project-form/{id}','ProjectController@editableProjectForm');
Route::post('/manan-administration2018/project/update-project','ProjectController@updateProject');
Route::get('/manan-administration2018/project/delete-project/{id}','ProjectController@deleteProject');
Route::get('/manan-administration2018/project/view-project/{id}','ProjectController@viewProject');
Route::get('/manan-administration2018/project/delete-sub-image/{id}','ProjectController@deleteSubImages');


//About Event Category
Route::get('/manan-administration2018/event-category/category-event-form','EventCategoryController@eventCategoryForm');
Route::post('/manan-administration2018/event-category/save-category-event','EventCategoryController@saveCategoryEvent');
Route::get('/manan-administration2018/event-category/manage-category-event','EventCategoryController@manageCategoryEvent');
Route::get('/manan-administration2018/event-category/unpublished-category-event/{id}','EventCategoryController@unpublishedCategoryEvent');
Route::get('/manan-administration2018/event-category/published-category-event/{id}','EventCategoryController@publishedCategoryEvent');
Route::get('/manan-administration2018/event-category/editable-category-event-form/{id}','EventCategoryController@editableCategoryEventForm');
Route::post('/manan-administration2018/event-category/update-category-event','EventCategoryController@updateCategoryEvent');
Route::get('/manan-administration2018/event-category/delete-category-event/{id}','EventCategoryController@deleteCategoryEvent');
Route::get('/manan-administration2018/event-category/view-event-category/{id}','EventCategoryController@categoryEventDetails');


//About Event
Route::get('/manan-administration2018/event/event-form','EventController@eventForm');
Route::post('/manan-administration2018/event/create-new-event','EventController@createNewEvent');
Route::get('/manan-administration2018/event/manage-event','EventController@manageEvent');
Route::get('/manan-administration2018/event/unpublished-event/{id}','EventController@unpublishedEvent');
Route::get('/manan-administration2018/event/published-event/{id}','EventController@publishedEvent');
Route::get('/manan-administration2018/event/editable-event-form/{id}','EventController@editableEventForm');
Route::post('/manan-administration2018/event/update-event','EventController@updateEvent');
Route::get('/manan-administration2018/event/delete-event/{id}','EventController@deleteEvent');
Route::get('/manan-administration2018/event/view-event/{id}','EventController@viewEvent');


//About Blog Category
Route::get('/manan-administration2018/blog-category/blog-category-form','BlogCategoryController@blogCategoryForm');
Route::post('/manan-administration2018/blog-category/add-new-blog-category','BlogCategoryController@addNewBlogCategory');
Route::get('/manan-administration2018/blog-category/manage-blog-category','BlogCategoryController@manageBlogCategory');
Route::get('/manan-administration2018/blog-category/unpublished-blog-category/{id}','BlogCategoryController@unpublishedBlogCategory');
Route::get('/manan-administration2018/blog-category/published-blog-category/{id}','BlogCategoryController@publishedBlogCategory');
Route::get('/manan-administration2018/blog-category/editable-blog-category-form/{id}','BlogCategoryController@editableBlogCategoryForm');
Route::post('/manan-administration2018/blog-category/update-blog-category-info','BlogCategoryController@updateBlogCategoryInfo');
Route::get('/manan-administration2018/blog-category/delete-blog-category/{id}','BlogCategoryController@deleteBlogCategoryInfo');
Route::get('/manan-administration2018/blog-category/view-blog-category/{id}','BlogCategoryController@viewBlogCategoryInfo');


//About Blog
Route::get('/manan-administration2018/blog/blog-post-form','BlogController@blogPostForm');
Route::post('/manan-administration2018/blog/blog-post','BlogController@blogPost');
Route::get('/manan-administration2018/blog/manage-blog','BlogController@manageBlog');
Route::get('/manan-administration2018/blog/unpublished-blog/{id}','BlogController@unpublishedBlogInfo');
Route::get('/manan-administration2018/blog/published-blog/{id}','BlogController@publishedBlogInfo');
Route::get('/manan-administration2018/blog/editable-blog-form/{id}','BlogController@editableBlogForm');
Route::post('/manan-administration2018/blog/update-blog','BlogController@updateBlogInfo');
Route::get('/manan-administration2018/blog/delete-blog/{id}','BlogController@deleteBlogInfo');
Route::get('/manan-administration2018/blog/view-blog/{id}','BlogController@viewBlogInfo');


//About Gallery
Route::get('/manan-administration2018/gallery/gallery-add-form','GalleryController@galleryAddForm');
Route::post('/manan-administration2018/gallery/add-image','GalleryController@imageAddToGallery');
Route::get('/manan-administration2018/gallery/manage-gallery','GalleryController@galleryManage');
Route::get('/manan-administration2018/gallery/unpublished-gallery-image/{id}','GalleryController@unpublishedGalleryImage');
Route::get('/manan-administration2018/gallery/published-gallery-image/{id}','GalleryController@publishedGalleryImage');
Route::get('/manan-administration2018/gallery/delete-gallery-image/{id}','GalleryController@deleteGalleryImage');

//About User Contact List
Route::get('/manan-administration2018/contact/contact-list','ContactController@manageContact');
Route::get('/manan-administration2018/contact/delete-contact/{id}','ContactController@contactDelete');
Route::get('/manan-administration2018/contact/view-contact-details/{id}','ContactController@contactDetailsPage');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//======== for frontend ==========

//home page
Route::get('/','WelcomeController@index');

//various
Route::get('/about-us','WelcomeController@aboutUs');
Route::get('/blog','WelcomeController@blog');

//Project starts
Route::get('/project/project-category-select/{id}','WelcomeController@selectProjectCategory');
Route::get('/project/all-project-categories','WelcomeController@allProjectCategories');
Route::get('/project/select-project/{id}','WelcomeController@selectProjectById');
//Project ends

//Event starts
Route::get('/event/all-event-categories','WelcomeController@allEventCategories');
Route::get('/event/event-category-select/{id}','WelcomeController@selectEventCategory');
Route::get('/event/select-event/{id}','WelcomeController@eventSelect');
//Event ends

//blog start
Route::get('/blog/all-blog-categories','WelcomeController@allBlogCategories');
Route::get('/blog/blog-category-select/{id}','WelcomeController@selectBlogCategory');
Route::get('/blog/select-blog/{id}','WelcomeController@selectBlogById');
//blog end

//Share Post Start
Route::get('/blog/select-blog/{id}/share','ShareController@shareFacebook');
//Share Post End

// Gallery
Route::get('/gallery/all-images','GalleryController@allGalleryImage');
Route::get('/gallery/project-image','GalleryController@projectGalleryPanel');
Route::get('/gallery/event-image','GalleryController@eventGalleryPanel');
Route::get('/gallery/blog-image','GalleryController@blogGalleryPanel');
Route::get('/gallery/other-image','GalleryController@otherGalleryPanel');

//Contact form
Route::post('/contact/contact-form-submit','ContactController@submitContactForm');
