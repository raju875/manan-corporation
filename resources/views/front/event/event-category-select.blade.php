@extends('front.master')

@section('title')
    <title>Manan Corporation | Project</title>
@endsection

@section('banner')
    @include('front.includes.banner',array('banners' => DB::table('banners')->where('publication_status',1)->get()))
@endsection

@section('body')
    <!--Publication Section Starts Here-->
    <section id="publication" class="animated fadeIn">
        <div class="container">
            <div class="row">
                <div class="text-center headline">
                    <h2 class="portfolio-heading wow animated fadeInLeft">
                        <a href="{{ url('/event/all-event-categories') }}">
                            Event/Category {{ $eventCategoryById->event_type }}
                        </a>
                    </h2>
                </div>
                <?php $i=0;?>
                @foreach($events as $event )
                    @if($i==0 )
                        <div class="wow animated fadeInLeft publication-img-left  col-xs-5 col-md-5 padding-bottom">
                            <a href="{{ url('/event/select-event/'.$event->id ) }}">
                                <img height="200" width="100%"  style="border: 2px solid #ff0000; padding: 7px; " class="l-img" src="{{ asset($event->photo) }}" alt="{{ $event->event_name }}">
                            </a>
                        </div>
                        <div class="wow animated fadeInRight col-xs-7 col-md-7 padding-bottom text-left">
                            <h4 class="publication-item-title">
                                <a href="{{ url('/event/select-event/'.$event->id ) }}">
                                    {{ $event->event_name }}
                                </a>
                            </h4>
                            <p>{{ substr(strip_tags($event->description ), 0, 100) }}
                                <a href="{{ url('/event/select-event/'.$event->id ) }}">
                                    {{ strlen(strip_tags($event->description )) > 70 ? " [...]ReadMore" : "" }}
                                </a>
                            </p>
                        </div>
                    @endif
                    @if($i==1 )
                        <div class="wow animated fadeInLeft col-xs-7 col-md-7 padding-bottom text-right">
                            <h4 class="publication-item-title">
                                <a href="{{ url('/event/select-event/'.$event->id ) }}">
                                    {{ $event->event_name }}
                                </a>
                            </h4>
                            <p>{{ substr(strip_tags($event->description ), 0, 100) }}
                                <a href="{{ url('/event/select-event/'.$event->id ) }}">
                                    {{ strlen(strip_tags($event->description )) > 70 ? " [...]ReadMore" : "" }}
                                </a>
                            </p>
                        </div>
                        <div class="wow animated fadeInRight publication-img-right  col-xs-5 col-md-5 padding-bottom">
                            <a href="{{ url('/event/select-event/'.$event->id ) }}">
                                <img height="200" width="100%" style="border: 2px solid #ff0000; padding: 7px; " class=" l-img" src="{{ asset($event->photo) }}" alt="{{ $event->event_name }}">
                            </a>
                        </div>
                        <?php $i = $i-2;?>
                    @endif
                    <?php $i++;?>
                @endforeach
            </div>
        </div>
    </section>
    <!--Publication Section Ends Here-->

@endsection