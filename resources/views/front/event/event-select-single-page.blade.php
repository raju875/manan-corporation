@extends('front.master')

@section('title')
    <title>Manan Corporation | Event</title>
@endsection

@section('body')
    <!--Single Pages content starts from here-->
    <section id="singel-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="banner-content">
                        <img class="img-full" src="{{ asset($eventById->photo) }}" alt="{{ $eventById->event_name }}">
                        <div class="banner-text">
                            <h3>{{ $eventById->event_name }}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container custom-padding-single">
            <div class="row">
                <div class="col-xs-12 col-md-8">
                    <div class="container-fluid">
                        <div class="text-content text-justify">
                            <?php echo $eventById->description ?>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-md-4 sidebar-content">
                    @if($countLogo==0 )
                        <a href="{{ url('/') }}">
                            <img class="circle" src="{{ asset('admin/logos/') }}/logo.png" alt="">
                        </a>
                    @else
                    @foreach($showLogoSinglePage as $showLogo )
                        <a class="navbar-brand" href="">
                            <img src="{{ asset($showLogo->logo) }}" 1width="80" height="60" class="d-inline-block align-top" alt="">
                        </a>
                    @endforeach
                    @endif
                        <h3 class="custom-border-bottom">
                            <a href="{{ url('/project/all-project-categories') }}">
                                Projects
                            </a>
                    </h3>
                    <ul class="sidebar">
                        @foreach($projectCategories as $projectCategory )
                            @foreach($projectRowCount as $count )
                                @if($projectCategory->id ==$count->category_id )
                                    <li>
                                        <a class="sidebar-link" href="{{ url('/project/project-category-select/'.$projectCategory->id) }}">
                                            {{ $projectCategory->category_name }}->{{ $count->total }}
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                        @endforeach
                    </ul>
                    <h3 class="custom-border-bottom">
                        <a href="{{ url('/event/all-event-categories') }}">
                            Events
                        </a>
                    </h3>
                    <ul class="sidebar">
                        @foreach($eventCategories as $eventCategory )
                            @foreach($eventRowCount as $count )
                                @if($eventCategory->id ==$count->event_category_id )
                                    <li>
                                        <a class="sidebar-link" href="{{ url('/event/event-category-select/'.$eventCategory->id) }}">
                                            {{ $eventCategory->event_type }}->{{ $count->total }}
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                        @endforeach
                    </ul>

                    <h3 class="custom-border-bottom">
                        <a href="{{ url('/blog/all-blog-categories') }}">
                            Blogs
                        </a>
                    </h3>
                    <ul class="sidebar">
                        @foreach($blogCategories as $blogCategory )
                            @foreach($blogRowCount as $count )
                                @if($blogCategory->id ==$count->blog_category_id )
                                    <li>
                                        <a class="sidebar-link" href="{{ url('/blog/blog-category-select/'.$blogCategory->id) }}">
                                            {{ $blogCategory->blog_category }}->{{ $count->total }}
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="share-section">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h4>Share this post</h4>
                    <ul style="padding-bottom: 70px;" class="nav justify-content-left social-padding">
                        <iframe src="https://www.facebook.com/plugins/share_button.php?href=http%3A%2F%2Fwww.manan.org.bd%2Fsingle.html&layout=button&size=small&mobile_iframe=true&appId=814680218727274&width=59&height=20" width="59" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                        <li class="nav-item">
                            <a class="nav-link active" href=""><i class="fab fa-twitter-square"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href=""><i class="fab fa-google-plus-square"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href=""><i class="fab fa-youtube-square"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="">
                                <i class="fab fa-linkedin"></i></a>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--Single Pages content Ends here-->
@endsection