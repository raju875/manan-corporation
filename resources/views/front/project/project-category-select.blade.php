@extends('front.master')

@section('title')
    <title>Manan Corporation | Project</title>
@endsection

@section('banner')
    @include('front.includes.banner',array('banners' => DB::table('banners')->where('publication_status',1)->get()))
@endsection

@section('body')
    <!--Project Section Starts Here-->
    <section id="project">
        <div class="container">
            <div class="text-center headline">
                <h2 class="portfolio-heading wow animated fadeInLeft">
                    <a href="{{ url('/project/select-project/') }}">
                        <h2 class="wow animated fadeInLeft">Project-Category {{ $selectCategoryName->category_name }}  </h2>
                    </a>
                </h2>
            </div>
            @foreach($projects->chunk(4) as $projects )
                <div class="row">
                    @foreach($projects as $project )
                        <div class="col-xs-12 col-sm-6 col-md-3 project-frame">
                            <a href="{{ url('/project/select-project/'.$project->id) }}">
                                <img height="240" width="100%" class="project-img"  src="{{ asset($project->image) }}" alt="Norway">
                                <div class="project-content">
                                    <p>{{ $project->project_name }}</p>
                                </div>
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
    </section>
    <!--Project Section Ends Here-->

    @endsection