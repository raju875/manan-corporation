@extends('front.master')

@section('title')
    <title>Manan Corporation | Blog</title>
@endsection

@section('banner')
    @include('front.includes.banner',array('banners' => DB::table('banners')->where('publication_status',1)->get()))
@endsection


@section('body')
    <!--Blog Section Starts Here-->
    <section class="content-section" id="blog">
        <div class="container">
            <div class="content-section-heading text-center headline">
                <h2 class="heading wow animated fadeInLeft">
                    <a href="{{ url('/blog/all-blog-categories') }}">
                        Blog/Category {{ "All" }}
                    </a>
                </h2>
            </div>
            @foreach($blogCategories->chunk(4) as $blogCategories )
                <div class="row no-gutters">
                    @foreach($blogCategories as $blogCategory )
                        <div class="col-lg-3">
                            <a class="blog-item" href="{{ url('/blog/blog-category-select/'.$blogCategory->id) }}">
                          <span class="caption">
                            <span class="caption-content">
                              <h2 style="font-size: 24px;">{{ $blogCategory->blog_category }}</h2>
                            </span>
                          </span>
                                <img class="img-fluid img-blog" src="{{ asset($blogCategory->image) }}" alt="{{ $blogCategory->blog_category }}">
                            </a>
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
    </section>
    <!--Blog Section Ends Here-->
@endsection