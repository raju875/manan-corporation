@extends('front.master')

@section('banner')
    @include('front.includes.banner',array('banners' => DB::table('banners')->where('publication_status',1)->get()))
@endsection

@section('title')
    <title>Manan Corporation | Project</title>
@endsection

@section('body')
    <!--Blog Section Starts Here-->
    <section class="content-section" id="blog">
        <div class="container">
            <div class="content-section-heading text-center headline">
                <h2 class="heading wow animated fadeInLeft">
                    <a href="{{ url('/blog/all-blog-categories') }}">
                        Blog/Category {{ $blogCategoryById->blog_category }}
                    </a>
                </h2>
            </div>
            @foreach($blogs->chunk(4) as $blogs )
                <div class="row no-gutters">
                    @foreach($blogs as $blog )
                        <div class="col-lg-3">
                            <a class="blog-item" href="{{ url('/blog/select-blog/'.$blog->id) }}">
                          <span class="caption">
                            <span class="caption-content">
                              <h2 style="font-size: 24px;">{{ $blog->blog_title }}</h2>
                            </span>
                          </span>
                                <img class="img-fluid img-blog" src="{{ asset($blog->image) }}" alt="{{ $blog->blog_title }}">
                            </a>
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
    </section>
    <!--Blog Section Ends Here-->
@endsection