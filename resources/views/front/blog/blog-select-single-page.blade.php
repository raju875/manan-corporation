@extends('front.master')

@section('title')
    <title>Manan Corporation | Project</title>
@endsection

@section('body')
    {{--Single Page Starts from here--}}
    <section id="single-page">
        <div class="container-fluid">
            <div class="row">

                <div class="col-8 left-side no-padding">
                    <div class="banner-content">
                        <img class="img-fluid banner-img" src="{{ asset($selectBlog->image) }}" alt="{{ $selectBlog->blog_title }}">
                    </div>
                </div>

                <div class="col-4 right-side">
                    <div class="calendar-wrapper">
                        <button id="btnPrev" type="button"><span class="btn-calendar"> < </span> Prev</button>
                        <button id="btnNext" type="button">Next <span class="btn-calendar"> > </span></button>
                        <div id="divCal"></div>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-8 no-padding">
                   <div class="blog-content">

                          <div class="blog-title">
                              <h2>{{ $selectBlog->blog_title }}</h2>
                              <span class="left span-icon">
                                  <i class="fas fa-pen-nib"></i> {{ $selectBlog->writer }}
                              </span>
                              <span class="right span-icon">
                                  <i class="fas fa-calendar-alt"></i> {{ $selectBlog->submitted_date }}
                              </span>
                          </div>

                       <div class="outer-container">
                           <div class="inner-container">
                               <div class="element">
                                   <div class="blog-text left-col">
                                       <?php echo $selectBlog->description ?>
                                   </div>
                               </div>
                           </div>
                       </div>


                   </div>
               </div>

                <div class="col-4">
                    <div class="sidebar-content">
                        <div class="social-content">

                            <a href="">
                                <div class="social-button facebook">
                                <i class="fab fa-facebook-f"></i>
                            </div>
                            </a>

                            <a href="">
                                <div class="social-button twitter">
                                    <i class="fab fa-twitter"></i>
                                </div>
                            </a>

                            <a href="">
                                <div class="social-button youtube">
                                    <i class="fab fa-youtube"></i>
                                </div>
                            </a>

                            <a href="">
                                <div class="social-button linkedin">
                                    <i class="fab fa-linkedin-in"></i>
                                </div>
                            </a>

                        </div>
                    </div>
                    <div class="clr"></div>
                    <div class="tab-content">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Recent</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Popular</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Categories</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Tags</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">

                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <div class="content-box">
                                    <ul class="tab-ul">
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">M Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>

                                    </ul>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                <div class="content-box">
                                    <ul class="tab-ul">
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">M Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>

                                    </ul>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                <div class="content-box">
                                    <ul class="tab-ul">
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">M Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>

                                    </ul>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                <div class="content-box">
                                    <ul class="tab-ul">
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">M Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>
                                        <li class="list-item"><a href="">Science</a></li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </section>
    {{--Single Page Ends here--}}
@endsection