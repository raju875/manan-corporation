<section id="footer">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="footer-headline">
                    <h4>Address</h4>
                </div>
                <div class="footer-body">
                    <address>
                        <p><i class="fas fa-map-marker-alt"></i>28, North Road (vuter goli)</p>
                        <p><i class="fas fa-map-marker-alt"></i>Dhanmondi, Dhaka, 1205</p>
                    </address>
                    <address>
                        <p><i class="fas fa-envelope"></i>Mail: rooftop@mail.com</p>
                        <p><i class="fas fa-mobile-alt"></i>Phone: 012545550458</p>
                    </address>
                </div>
            </div>

            <div class="col">
                <div class="footer-headline">
                    <h4 class="text-right">Map</h4>
                </div>
                <div class="footer-body" style="margin-left: -140px; margin-right: 33px;">
                    <div class="embed-responsive embed-responsive-4by3 google-map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d322.8356921019261!2d90.41191808977032!3d23.730652353561535!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755b8f62f8cc0b5%3A0xa64c0ee92d9b8be4!2sH.+M.+Siddique+Mansion!5e0!3m2!1sbn!2sbd!4v1490030519319" width="600" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>

            <div class="col">
                <div class="footer-headline">
                    <h4>Contact</h4>
                </div>
                <div class="footer-body">
                    <?php date_default_timezone_set('Asia/Dhaka')?>
                    <form action="{{ url('/contact/contact-form-submit') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="text" placeholder="Name" name="name" class="form-control" id="name" required>
                            <span>{{ $errors->has('name') ? $errors->first('name') : ' ' }}</span>
                        </div>
                        <div class="form-group">
                            <input type="hidden"  name="date" class="form-control" value="{{ date("Y-m-d") }}" >
                        </div>
                        <div class="form-group">
                            <input type="hidden"  name="time" class="form-control" value={{ date(" H:i:s", time()) }}>
                        </div>
                        <div class="form-group">
                            <input type="email" placeholder="Email" name="email" class="form-control" id="email" required>
                            <span>{{ $errors->has('email') ? $errors->first('email') : ' ' }}</span>
                        </div>
                        <div class="form-group">
                            <textarea class="footer-textarea" name="topics" placeholder="Text Here..." id="" cols="55" rows="5" required></textarea>
                            <span>{{ $errors->has('topics') ? $errors->first('topics') : ' ' }}</span>
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>