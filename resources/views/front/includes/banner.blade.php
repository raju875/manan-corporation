<div id="jssor_1" style="position:relative;margin:0 auto;top:-3px;left:0px;width:980px;height:380px;overflow:hidden;visibility:hidden;">
    <!-- Loading Screen -->
    <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
    </div>
    <div data-u="slides" style="cursor:default;position:relative;top:2px;left:0px;width:980px;height:380px;overflow:hidden;">
        @foreach($banners as $banner )
            <div>
                <img width="100%" data-u="image" src="{{ asset($banner->banner) }}" />
                @if($banner->banner_title != null && $banner->description!= null  )
                    <div class="carousel-caption d-none d-md-block">
                        <h3>{{ $banner->banner_title }}</h3>
                        <p>{{ $banner->description }}</p>
                    </div>
                @endif
                @if($banner->banner_title != null && $banner->description== null  )
                    <div class="carousel-caption d-none d-md-block">
                        <h3>{{ $banner->banner_title }}</h3>
                    </div>
                @endif
                @if($banner->banner_title == null && $banner->description!= null  )
                    <div class="carousel-caption d-none d-md-block">
                        <p><?php echo $banner->description ?></p>
                    </div>
                @endif
            </div>
        @endforeach
    </div>
    <!-- Bullet Navigator -->
    <div data-u="navigator" class="jssorb051" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
        <div data-u="prototype" class="i" style="width:16px;height:16px;">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <circle class="b" cx="8000" cy="8000" r="5800"></circle>
            </svg>
        </div>
    </div>
    <!-- Arrow Navigator -->
    <div data-u="arrowleft" class="jssora051" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
        <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
            <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
        </svg>
    </div>
    <div data-u="arrowright" class="jssora051" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
        <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
            <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
        </svg>
    </div>
</div>