<section id="top-nav">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm">
                <nav class="navbar navbar-expand-lg navbar-light bg-custom-nav-top">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link active" href="{{ url('/gallery/all-images') }}">Gallery</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#footer">Contact Us</a>
                        </li>
                    </ul>
                    <ul class="nav justify-content-right social-padding">
                        <li class="nav-item">
                            <a class="nav-link active" href=""><i class="fab fa-facebook-square"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href=""><i class="fab fa-twitter-square"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href=""><i class="fab fa-google-plus-square"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href=""><i class="fab fa-youtube-square"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="">
                                <i class="fab fa-linkedin"></i></a>
                        </li>

                    </ul>
                </nav>
            </div>
        </div>
    </div>
</section>