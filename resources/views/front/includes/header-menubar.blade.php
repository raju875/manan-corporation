<section id="nav-bottom">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm">
                <nav class="sticky-top navbar navbar-expand-lg navbar-light bg-custom-nav-bottom  fixed-top">
                    @if($countLogo==0 )
                        <a class="navbar-brand" href="">
                            <img  src="{{ asset('admin/logos/') }}/logo.png" 1width="80" height="60" class="d-inline-block align-top" alt="">
                        </a>
                    @else
                    @foreach($showLogoMenuBar as $logo )
                            <a class="navbar-brand" href="">
                                <img src="{{ asset($logo->logo) }}" 1width="80" height="60" class="d-inline-block align-top" alt="">
                            </a>
                        @endforeach
                    @endif
                    <a href="" class="nav-link nav-brand-link">Roof Garden</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto ml-custom">
                            <li class="nav-item active">
                                <a class="nav-link" href="{{ url('/') }}">Home<span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('/about-us') }}">About Us</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Project
                                </a>
                                <div class="dropdown-menu project" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item aside" href="{{ url('/project/all-project-categories') }}">{{ "All" }}
                                        {{--<div class="dropdown-divider"></div>--}}</a>
                                    @foreach($projectCategories as $projectCategory )
                                        <a class="dropdown-item aside" href="{{ url('/project/project-category-select/'.$projectCategory->id) }}">{{ $projectCategory->category_name }}</a>
                                        {{--<div class="dropdown-divider"></div>--}}
                                    @endforeach
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Publication
                                </a>
                                <div class="dropdown-menu publication" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item aside " href="{{ url('/event/all-event-categories') }}">{{ "All" }}
                                        {{--<div class="dropdown-divider"></div></a>--}}
                                @foreach($eventCategories as $eventCategory )
                                    <a class="dropdown-item aside" href="{{ url('/event/event-category-select/'.$eventCategory->id) }}">{{ $eventCategory->event_type }}</a>
                                    {{--<div class="dropdown-divider"></div>--}}
                                        @endforeach
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Blog
                                </a>
                                <div class="dropdown-menu blog" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item aside " href="{{ url('/blog/all-blog-categories') }}">{{ "All" }}
                                        {{--<div class="dropdown-divider"></div>--}}</a>
                                    @foreach($blogCategories as $blogCategory )
                                        <a class="dropdown-item aside" href="{{ url('/blog/blog-category-select/'.$blogCategory->id) }}">{{ $blogCategory->blog_category }}</a>
                                        {{--<div class="dropdown-divider">--}}{{--</div>--}}
                                    @endforeach
                                </div>
                            </li>
                        </ul>
                        <form class="form-inline my-2 my-lg-0">
                            <input class="form-control custom-form mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                        </form>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</section>