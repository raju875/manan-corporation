@extends('front.master')

@section('title')
    <title>Manan Corporation | Gallery</title>
@endsection

@section('banner')
    @include('front.includes.banner',array('banners' => DB::table('banners')->where('publication_status',1)->get()))
@endsection

@section('body')
    <div class="container">
        <div class="row">
            <div class="gallery col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h1 class="gallery-title">Gallery</h1>
            </div>
            <div align="center " class="button-row">
                <a href="{{ url('/gallery/all-images') }}">
                    <button class="btn btn-default filter-button" data-filter="all">All</button>
                </a>
                <a href="{{ url('/gallery/project-image') }}">
                    <button class="btn btn-default filter-button" data-filter="projects">Projects</button>
                </a>
                <a href="{{ url('/gallery/event-image') }}">
                    <button class="btn btn-default filter-button" data-filter="Events">Events</button>
                </a>
                <a href="{{ url('/gallery/blog-image') }}">
                    <button class="btn btn-default filter-button" data-filter="blog">Blog</button>
                </a>
                <a href="{{ url('/gallery/other-image') }}">
                    <button class="btn btn-default filter-button" data-filter="blog">Other</button>
                </a>
            </div>
            <div class="clear"></div>
            <?php $i=0 ?>
            <div class="row">
                @foreach($projects as $project )
                    @if($i==3 )
                        <?php $i = 0 ?>
                        <?php echo "<br>" ?>
                    @endif
                    @if($i<3 )
                        <div class="col-sm-4 col-md-4">
                            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter projects">
                                <img src="{{ asset($project->image) }}" width="300px" height="230px" />
                            </div>
                            <?php $i++ ?>
                        </div>
                    @endif

                @endforeach
            </div>

        </div>
@endsection