@extends('front.master')

@section('title')
    <title>Rooftop Gardening</title>
    @endsection

@section('banner')
    @include('front.includes.banner',array('banners' => DB::table('banners')->where('publication_status',1)->get()))
    @endsection

@section('body')
    <!--About Section Starts Here-->
    <section id="about">
        <div class="container">
            @if(Session::has('message'))
                <h3 class="text text-center text-success">{{ Session::get('message') }}</h3>
            @endif
            <div class="row">
                <div class="col">
                    <div class="text-center headline">
                        <h2 class="wow animated fadeInLeft">
                            <a href="{{ url('/about-us') }}">About Us</a>
                        </h2>
                    </div>
                    @foreach($abouts as $about )
                        <div  class="wow animated zoomIn paragraph text-center">
                            <p><?php echo $about->description ?></p>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <!--About Section Ends Here-->

    <!--Project Section Starts Here-->
   <section id="project">
       <div class="container">
           <div class="text-center headline">
               <h2 class="portfolio-heading wow animated fadeInLeft">
                   <a href="{{ url('/project/all-project-categories') }}">
                       Our Projects
                   </a>
               </h2>
           </div>
           @foreach($projects->chunk(4) as $projects )
           <div class="row">
               @foreach($projects as $project )
               <a href="{{ url('/project/select-project/'.$project->id) }}">
               <div class="col-xs-12 col-sm-6 col-md-3 project-frame">
                   <img height="240" width="100%" class="project-img" src="{{ asset($project->image) }}" alt="Norway">
                   <div class="project-content">
                       <a class="project-text" href="{{ url('/project/select-project/'.$project->id) }}"> <p>{{ $project->project_name }}</p></a>
                   </div>
               </div>-
               </a>
               @endforeach
           </div>
           @endforeach
       </div>
   </section>
    <!--Project Section Ends Here-->

        <!--Publication Section Starts Here-->
        <section id="publication" class="animated fadeIn">
            <div class="container">
                <div class="row">
                    <div class="text-center headline">
                        <h2 class="portfolio-heading wow animated fadeInLeft">
                          <a href="{{ url('/event/all-event-categories') }}">
                              Publication
                             </a>
                        </h2>
                    </div>
                    <?php $i=0;?>
                        @foreach($events as $event )
                            @if($i==0 )
                            <div class="wow animated fadeInLeft publication-img-left  col-xs-5 col-md-5 padding-bottom">
                               <a href="{{ url('/event/select-event/'.$event->id ) }}">
                                   <img height="200" width="100%"  style="border: 2px solid #ff0000; padding: 7px; " class=" l-img" src="{{ asset($event->photo) }}" alt="{{ $event->event_name }}">
                               </a>
                            </div>
                            <div class="wow animated fadeInRight col-xs-7 col-md-7 padding-bottom text-left">
                                <h4 class="publication-item-title">
                                    <a href="{{ url('/event/select-event/'.$event->id ) }}">
                                    {{ $event->event_name }}
                                    </a>
                                </h4>
                                <p>{{ substr(strip_tags($event->description ), 0, 100) }}
                                    <a href="{{ url('/event/select-event/'.$event->id ) }}">
                                        {{ strlen(strip_tags($event->description )) > 70 ? " [...]ReadMore" : "" }}
                                    </a>
                                </p>
                            </div>
                        @endif
                    @if($i==1 )
                        <div class="wow animated fadeInLeft col-xs-7 col-md-7 padding-bottom text-right">
                            <h4 class="publication-item-title">
                                <a href="{{ url('/event/select-event/'.$event->id ) }}">
                                {{ $event->event_name }}
                                </a>
                            </h4>
                            <p>{{ substr(strip_tags($event->description ), 0, 100) }}
                                <a href="{{ url('/event/select-event/'.$event->id ) }}">
                                    {{ strlen(strip_tags($event->description )) > 70 ? " [...]ReadMore" : "" }}
                                </a>
                            </p>
                        </div>
                        <div class="wow animated fadeInRight publication-img-right  col-xs-5 col-md-5 padding-bottom">
                            <a href="{{ url('/event/select-event/'.$event->id ) }}">
                            <img height="200" width="100%" style="border: 2px solid #ff0000; padding: 7px; " class="l-img" src="{{ asset($event->photo) }}" alt="{{ $event->event_name }}">
                            </a>
                        </div>
                                    <?php $i = $i-2;?>
                                @endif
                                <?php $i++;?>
                    @endforeach
                </div>
            </div>
        </section>
        <!--Publication Section Ends Here-->
<!--Blog Section Starts Here-->
    <section class="content-section" id="blog">
        <div class="container">
            <div class="content-section-heading text-center headline">
                <h2 class="heading wow animated fadeInLeft">
                    <a href="{{ url('/blog/all-blog-categories') }}">
                        Blog
                    </a>
                </h2>
            </div>
            @foreach($blogs->chunk(4) as $blog_item)
            <div class="row no-gutters">
                @foreach($blog_item as $blog)
                <div class="col-lg-3">
                    <a class="blog-item" href="{{ url('/blog/select-blog/'.$blog->id) }}">
                          <span class="caption">
                            <span class="caption-content">
                              <h2>{{ $blog ->blog_title }}</h2>
                                <span class="text-center"><i class="fas fa-user-tie"></i>{{ $blog->writer }}</span>
                                <span class="text-center"><i class="fas fa-folder"></i>{{ $blog->blog_category }}</span>
                                <span class="text-center"><i class="fas fa-calendar-alt"></i>{{ $blog->submitted_date }}</span>
                            </span>
                          </span>
                        <img class="img-fluid img-blog" src="{{ $blog->image }}" alt="">
                    </a>
                </div>
                @endforeach
            </div>
            @endforeach
        </div>
    </section>
    <!--Blog Section Ends Here-->

@endsection
