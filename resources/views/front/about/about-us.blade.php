@extends('front.master')

@section('banner')
    @include('front.includes.banner',array('banners' => DB::table('banners')->where('publication_status',1)->get()))
    @endsection

@section('title')
    <title>Manan Corporation | AboutUs</title>
    @endsection

@section('body')
    <!--Slider Ends Here-->
    <!--About Section Starts Here-->
    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="text-center headline">
                        <h2 class="wow animated fadeInLeft">About Us</h2>
                        <hr class="hr-custom hr-about wow fadeInRight">
                    </div>
                    <div class="wow animated zoomIn paragraph text-center">
                       @foreach($abouts as $about )
                            <?php echo $about->description ?>
                           @endforeach
                    </div>
                </div>
            </div>
            <div class="hr-animation">
                <hr class="wow fadeInRight">
            </div>
        </div>
    </section>
    @endsection