@extends('admin.master')

@section('title')
    <title>Manan Corporation | Category Error</title>
@endsection
@section('body')
    <section class="content-header">
        <h1>
            500 Error Page
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">500 error</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="error-page">
            <h2 class="headline text-red">500</h2>

            <div class="error-content">
                <h3><i class="fa fa-warning text-red"></i> Oops! Something went wrong.</h3>
                   <h4 class="text-danger">{{ $aleart }}</h4>
            </div>
        </div>
        <!-- /.error-page -->

    </section>
@endsection