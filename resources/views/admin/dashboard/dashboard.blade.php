@extends('admin.master')

@section('title')
    <title>Manan Corporation | Dashboard</title>
    @endsection

@section('body')
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <a href="{{ url('manan-administration2018/banner/banner-manage') }}"><h3>Banner</a>

                            </h3>
                            <a href="{{ url('manan-administration2018/banner/banner-manage') }}"> <p> Banner Information </p></a>
                        </div>
                        <a href="{{ url('manan-administration2018/banner/banner-manage') }}">
                            <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        </a>
                        <a href="{{ url('manan-administration2018/banner/banner-manage') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <a href="{{ url('manan-administration2018/about/manage-about') }}">
                        <div class="inner">
                            <a href="{{ url('manan-administration2018/about/manage-about') }}">
                                <h3>AboutUs</h3>
                            </a>
                            <a href="{{ url('manan-administration2018/about/manage-about') }}">
                                <p>Details AboutUs</p>
                            </a>
                        </div>
                        <a href="{{ url('manan-administration2018/about/manage-about') }}">
                            <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        </a>
                        <a href="{{ url('manan-administration2018/about/manage-about') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                    </a>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <a href="{{ url('/manan-administration2018/project/manage-project') }}">
                                <h3>Project</h3>
                            </a>
                            <a href="{{ url('/manan-administration2018/project/manage-project') }}">
                                <p>Project Details</p>
                            </a>
                        </div>
                        <a href="{{ url('/manan-administration2018/project/manage-project') }}">
                            <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        </a>
                        <a href="{{ url('/manan-administration2018/project/manage-project') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <a href="{{ url('/manan-administration2018/event/manage-event') }}">
                                <h3>Events</h3>
                            </a>
                            <a href="{{ url('/manan-administration2018/event/manage-event') }}">
                                <p>Event Information</p>
                            </a>
                        </div>
                        <a href="{{ url('/manan-administration2018/event/manage-event') }}">
                            <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        </a>
                        <a href="{{ url('/manan-administration2018/event/manage-event') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <a href="{{ url('/manan-administration2018/blog/manage-blog') }}">
                                <h3>Blogs</h3>
                            </a>
                            <a href="{{ url('/manan-administration2018/blog/manage-blog') }}">
                                <p>Blog Details</p>
                            </a>
                        </div>
                        <a href="{{ url('/manan-administration2018/blog/manage-blog') }}">
                            <div class="icon">
                            <i class="ion-ios-book"></i>
                        </div>
                        </a>
                        <a href="{{ url('/manan-administration2018/blog/manage-blog') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>53<sup style="font-size: 20px">%</sup></h3>

                            <p>Bounce Rate</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3>44</h3>

                            <p>User Registrations</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>65</h3>

                            <p>Unique Visitors</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            <!-- /.row -->
            <!-- Main row -->
            <div class="row">
                <div id="map" style="width:100%;height:400px;"></div>
                <!-- Left col -->
                {{--<section class="col-lg-7 connectedSortable">--}}
                    {{--<!-- Custom tabs (Charts with tabs)-->--}}
                    {{--<div class="nav-tabs-custom">--}}
                        {{--<!-- Tabs within a box -->--}}
                        {{--<ul class="nav nav-tabs pull-right">--}}
                            {{--<li class="active"><a href="#revenue-chart" data-toggle="tab">Area</a></li>--}}
                            {{--<li><a href="#sales-chart" data-toggle="tab">Donut</a></li>--}}
                            {{--<li class="pull-left header"><i class="fa fa-inbox"></i> Sales</li>--}}
                        {{--</ul>--}}
                        {{--<div class="tab-content no-padding">--}}
                            {{--<!-- Morris chart - Sales -->--}}
                            {{--<div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;"></div>--}}
                            {{--<div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;"></div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="box box-solid bg-green-gradient">--}}
                        {{--<div class="box-header">--}}
                            {{--<i class="fa fa-calendar"></i>--}}

                            {{--<h3 class="box-title">Calendar</h3>--}}
                            {{--<!-- tools box -->--}}
                            {{--<div class="pull-right box-tools">--}}
                                {{--<!-- button with a dropdown -->--}}
                                {{--<div class="btn-group">--}}
                                    {{--<button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">--}}
                                        {{--<i class="fa fa-bars"></i></button>--}}
                                    {{--<ul class="dropdown-menu pull-right" role="menu">--}}
                                        {{--<li><a href="#">Add new event</a></li>--}}
                                        {{--<li><a href="#">Clear events</a></li>--}}
                                        {{--<li class="divider"></li>--}}
                                        {{--<li><a href="#">View calendar</a></li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                                {{--<button type="button" class="btn btn-success btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>--}}
                                {{--</button>--}}
                                {{--<button type="button" class="btn btn-success btn-sm" data-widget="remove"><i class="fa fa-times"></i>--}}
                                {{--</button>--}}
                            {{--</div>--}}
                            {{--<!-- /. tools -->--}}
                        {{--</div>--}}
                        {{--<!-- /.box-header -->--}}
                        {{--<div class="box-body no-padding">--}}
                            {{--<!--The calendar -->--}}
                            {{--<div id="calendar" style="width: 100%"></div>--}}
                        {{--</div>--}}
                        {{--<!-- /.box-body -->--}}

                    {{--</div>--}}
                    {{--<!-- /.box -->--}}

                {{--</section>--}}
                <!-- /.Left col -->
                <!-- right col (We are only adding the ID to make the widgets sortable)-->
                <!-- right col -->
            </div>
            <!-- /.row (main row) -->

        </section>
        <!-- /.content -->
    @endsection