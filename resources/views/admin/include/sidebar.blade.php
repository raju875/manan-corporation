<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('admin/loginProfileImages/') }}/{{ Auth::user()->avatar }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="treeview">
                    <li>
                <a href="{{ url('/manan-administration2018/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-files-o"></i>
                    <span>Logo</span>
                    <span class="pull-right-container">
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/manan-administration2018/logo/logo-add-form') }}"><i class="fa fa-circle-o"></i>  Logo Add</a></li>
                    <li><a href="{{ url('manan-administration2018/logo/logo-manage') }}"><i class="fa fa-circle-o"></i> Logo Manage</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-files-o"></i>
                    <span>Banner</span>
                    <span class="pull-right-container">
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/manan-administration2018/banner/banner-add-form') }}"><i class="fa fa-circle-o"></i>  Banner Add</a></li>
                    <li><a href="{{ url('manan-administration2018/banner/banner-manage') }}"><i class="fa fa-circle-o"></i> Banner Manage</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-files-o"></i>
                    <span>About Us</span>
                    <span class="pull-right-container">
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/manan-administration2018/about/about-form') }}"><i class="fa fa-circle-o"></i> Add New About</a></li>
                    <li><a href="{{ url('manan-administration2018/about/manage-about') }}"><i class="fa fa-circle-o"></i> Manage About</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-files-o"></i>
                    <span>Project</span>
                    <span class="pull-right-container">
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/manan-administration2018/project-category/category-project-form') }}"><i class="fa fa-circle-o"></i> Add New Project Category</a></li>
                    <li><a href="{{ url('/manan-administration2018/project-category/manage-category-project') }}"><i class="fa fa-circle-o"></i> Manage Project Category</a></li>
                    <li><a href="{{ url('/manan-administration2018/project/project-form') }}"><i class="fa fa-circle-o"></i> Add New Project</a></li>
                    <li><a href="{{ url('/manan-administration2018/project/manage-project') }}"><i class="fa fa-circle-o"></i> Manage Project</a></li>
                </ul>
            </li>
            </li> <li class="treeview">
                <a href="#">
                    <i class="fa fa-files-o"></i>
                    <span>Event </span>
                    <span class="pull-right-container">
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/manan-administration2018/event-category/category-event-form') }}"><i class="fa fa-circle-o"></i> Add New Event Category</a></li>
                    <li><a href="{{ url('/manan-administration2018/event-category/manage-category-event') }}"><i class="fa fa-circle-o"></i> Event Category Manage</a></li>
                    <li><a href="{{ url('/manan-administration2018/event/event-form') }}"><i class="fa fa-circle-o"></i> Create Event</a></li>
                    <li><a href="{{ url('/manan-administration2018/event/manage-event') }}"><i class="fa fa-circle-o"></i> Manage Event</a></li>
                </ul>
            </li>
            </li>
            </li> <li class="treeview">
                <a href="#">
                    <i class="fa fa-files-o"></i>
                    <span>Blog </span>
                    <span class="pull-right-container">
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/manan-administration2018/blog-category/blog-category-form') }}"><i class="fa fa-circle-o"></i> Add New Blog Category</a></li>
                    <li><a href="{{ url('/manan-administration2018/blog-category/manage-blog-category') }}"><i class="fa fa-circle-o"></i> Manage Blog Category</a></li>
                    <li><a href="{{ url('/manan-administration2018/blog/blog-post-form') }}"><i class="fa fa-circle-o"></i> Blog Post</a></li>
                    <li><a href="{{ url('/manan-administration2018/blog/manage-blog') }}"><i class="fa fa-circle-o"></i> Blog Manage</a></li>
                </ul>
            </li>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-files-o"></i>
                    <span>Gallery</span>
                    <span class="pull-right-container">
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/manan-administration2018/gallery/gallery-add-form') }}"><i class="fa fa-dashboard"></i> Image Add </a></li>
                    <li><a href="{{ url('/manan-administration2018/gallery/manage-gallery') }}"><i class="fa fa-dashboard"></i> Gallery Manage </a></li>
                </ul>
            </li>
            <li class="treeview">
            <li>
                <a href="{{ url('/manan-administration2018/contact/contact-list') }}"><i class="fa fa-dashboard"></i> Contact List</a></li>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>