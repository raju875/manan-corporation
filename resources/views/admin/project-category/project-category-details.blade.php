@extends('admin.master')

@section('title')
    <title>Manan Corporation | Project Category </title>
@endsection

@section('body')
    <div class="col-md-11" style="margin:50px 0px 0px 100px">
        <h2 class="box-title text-center">Project Category Information</h2>
        <hr>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Project Category Information</h3>
                            <h3 class="text text-success text-center"></h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <table class="table table-bordered table-hover">
                                    <tr>
                                        <th class="col-sm-3">Category Name</th>
                                        <td class="col-sm-9">
                                            {{ $projectCategoryById->category_name }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-3"> Image</th>
                                        <td class="col-sm-9">
                                            <img src="{{ asset( $projectCategoryById->category_image ) }}" height="200" width="225">
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-3">Description</th>
                                        <td class="col-sm-9">
                                            <?php echo $projectCategoryById->description ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-3">Publication Status</th>
                                        <td class="col-sm-9">
                                            @if($projectCategoryById->publication_status == 1)
                                                {{'Published'}}
                                            @else
                                                {{ 'Unpublished' }}
                                            @endif
                                        </td>
                                    </tr>
                                </table>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>

        <div class="control-sidebar-bg"></div>
    </div>

@endsection