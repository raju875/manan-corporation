@extends('admin.master')

@section('title')
    <title>Manan Corporation |  Category Table</title>
@endsection

@section('body')

    <div class="col-md-11" style="margin:50px 0px 0px 100px">
        <h2 class="box-title text-center">Category Table</h2>
        <hr>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Manage Category</h3>
                            @if($message = Session::get('message'))
                                <h3 class="text text-success text-center">{{ $message }}</h3>
                            @endif
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>SL No</th>
                                    <th>SL Id</th>
                                    <th>Category Name</th>
                                    <th>Category Image</th>
                                    <th>Description</th>
                                    <th>Publication Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <?php $sl=1 ?>
                                @foreach($categories as $categorie )
                                    <tbody>
                                    <tr>
                                        <td>{{ $sl }}</td>
                                        <td>{{ $categorie->id }}</td>
                                        <td>{{ $categorie->category_name }}</td>
                                        <td><img src="{{ asset($categorie->category_image ) }}"  height="80" width="105"></td>
                                        <td>
                                            @if($categorie->description==null )
                                                {{'NULL'}}
                                            @else
                                            {{ substr(strip_tags($categorie->description ), 0, 100) }}
                                            <a href="{{ url('/manan-administration2018/project-category/view-project-category/'.$categorie->id) }}">
                                                {{ strlen(strip_tags($categorie->description )) > 40 ? " [...]ReadMore" : "" }}
                                            </a>
                                            @endif
                                        </td>
                                        <td>
                                            @if($categorie->publication_status == 1)
                                                {{'Published'}}
                                            @else
                                                {{ 'Unpublished' }}
                                            @endif
                                        </td>

                                        <td>
                                            @if($categorie->publication_status == 1)
                                                <a href="{{ url('/manan-administration2018/project-category/unpublished-category-project/'.$categorie->id) }}" class="btn btn-success btn-xs" title="published">
                                                    <span class="glyphicon glyphicon-arrow-up"></span>
                                                </a>
                                            @else
                                                <a href="{{ url('/manan-administration2018/project-category/published-category-project/'.$categorie->id) }}" class="btn btn-warning btn-xs" title="unpublished">
                                                    <span class="glyphicon glyphicon-arrow-down"></span>
                                                </a>
                                            @endif

                                            <a href="{{  url('/manan-administration2018/project-category/editable-category-project-form/'.$categorie->id) }}" class="btn btn-primary btn-xs" title="edit">
                                                <span class="glyphicon glyphicon-edit"></span>
                                            </a>
                                            <a href="{{   url('/manan-administration2018/project-category/delete-category-project-info/'.$categorie->id) }}" onclick="return confirm('Are you sure to delete it !!!')" class="btn btn-primary btn-xs" title="delete">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </a><a href="{{   url('/manan-administration2018/project-category/view-project-category/'.$categorie->id) }}"  class="btn btn-primary btn-xs" title="details">
                                                <span class="glyphicon glyphicon-search"></span>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php $sl++ ?>
                                    </tbody>
                                @endforeach
                            </table>
                            <nav class="justify-content-end">
                                {{ $categories->links() }}
                            </nav>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>

        <div class="control-sidebar-bg"></div>
    </div>

@endsection