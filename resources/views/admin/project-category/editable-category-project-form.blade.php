@extends('admin.master')

@section('title')
    <title> Manan Corporation | Editable Category Form</title>
@endsection

@section('body')

    <div class="col-md-10" style="margin:50px 0px 0px 100px">
        <h2 class="box-title text-center">Editable Category Form</h2>
        <hr>
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Update Category Info</h3>
                @if(Session::has('message'))
                    <h3 class="text text-center text-success">{{ Session::get('message') }}</h3>
                @endif
            </div>

            <!-- /.box-header -->
            <!-- form start -->

            @foreach($categoryInfos as $categoryInfo)

                <form class="form-horizontal" action="{{ url('/manan-administration2018/project-category/update-category-project')}}" method="POST" enctype="multipart/form-data" >
                    {{ csrf_field() }}
                    <?php date_default_timezone_set('Asia/Dhaka')?>
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-sm-10">
                                <input type="hidden" class="form-control" name="id" value="{{ $categoryInfo->id }}">
                            </div>
                            <span>{{ $errors->has('id') ? $errors->first('id') : ' ' }}</span>

                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" >Category Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" readonly name="category_name" value="{{ $categoryInfo->category_name }}" required>
                            </div>
                            <span>{{ $errors->has('category_name') ? $errors->first('category_name') : ' ' }}</span>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-2 control-label">Category Image</label>
                            <div class="col-sm-10">
                                <input type="file" name="category_image">
                                <br>
                                <img src="{{ asset($categoryInfo->category_image) }}" height="80" width="80">
                                <span style="color: red">{{ $errors->has('category_image') ? $errors->first('category_name') : ' ' }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Description</label>
                            <div class="col-sm-10">
                            <textarea id="description" name="description" class="form-control" rows="50" cols="8">
                                {{ $categoryInfo->description}}
                    </textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">
                                <select class="form-control" name="publication_status" required>
                                    @if($categoryInfo->publication_status == 1 )
                                        <option value="1">Published</option>
                                        <option value="0">Unpublished</option>
                                    @else
                                        <option value="0">Unpublished</option>
                                        <option value="1">Published</option>
                                    @endif
                                </select>
                            </div>
                            <span style="color: red">{{ $errors->has('publication_status') ? $errors->first('publication_status') : ' ' }}</span>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2">
                            </div>
                            <div class="col-sm-10">
                                <button type="submit" name="btn" class="btn btn-info btn-block">Update It </button>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <!-- /.box-body -->

                </form>
        </div>
        <div class="control-sidebar-bg"></div>
    </div>

@endsection

@section('js')
    <script>
        CKEDITOR.replace( 'description',{
            extraPlugins: 'codesnippet'
        });
    </script>
@endsection