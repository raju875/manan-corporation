@extends('admin.master')

@section('title')
    <title>Manan Corporation |  Event Type Table</title>
@endsection

@section('body')

    <div class="col-md-11" style="margin:50px 0px 0px 100px">
        <h2 class="box-title text-center">Event-Type Table</h2>
        <hr>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Manage Event-Type</h3>
                            @if($message = Session::get('message'))
                                <h3 class="text text-success text-center">{{ $message }}</h3>
                            @endif
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>SL No</th>
                                    <th>Event Type</th>
                                    <th>Description</th>
                                    <th>Image</th>
                                    <th>Publication Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <?php $sl=1 ?>
                                @foreach($eventCategories as $eventCategory )
                                    <tbody>
                                    <tr>
                                        <td>{{ $sl }}</td>
                                        <td>{{ $eventCategory->event_type }}</td>
                                        <td>
                                            @if($eventCategory->description == null )
                                                {{ 'NULL' }}
                                            @else
                                                {{ substr(strip_tags($eventCategory->description), 0, 50) }}
                                                <a href="{{ url('/manan-administration2018/event-category/view-event-category/'.$eventCategory->id) }}">
                                                    {{ strlen(strip_tags($eventCategory->description)) > 20 ? " ...ReadMore" : "" }}
                                                </a>
                                            @endif
                                        </td>
                                        <td> <img src="{{ asset( $eventCategory->image ) }}" height="80" width="100"></td>
                                        <td>
                                            @if($eventCategory->publication_status == 1)
                                                {{'Published'}}
                                            @else
                                                {{ 'Unpublished' }}
                                            @endif
                                        </td>

                                        <td>
                                            @if($eventCategory->publication_status == 1)
                                                <a href="{{ url('/manan-administration2018/event-category/unpublished-category-event/'.$eventCategory->id) }}" class="btn btn-success btn-xs" title="published">
                                                    <span class="glyphicon glyphicon-arrow-up"></span>
                                                </a>
                                            @else
                                                <a href="{{  url('/manan-administration2018/event-category/published-category-event/'.$eventCategory->id) }}" class="btn btn-warning btn-xs" title="unpublished">
                                                    <span class="glyphicon glyphicon-arrow-down"></span>
                                                </a>
                                            @endif

                                            <a href="{{  url('/manan-administration2018/event-category/editable-category-event-form/'.$eventCategory->id) }}" class="btn btn-primary btn-xs" title="edit">
                                                <span class="glyphicon glyphicon-edit"></span>
                                            </a>
                                            <a href="{{  url('/manan-administration2018/event-category/delete-category-event/'.$eventCategory->id) }}" onclick="return confirm('Are you sure to delete it !!!')" class="btn btn-primary btn-xs" title="delete">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </a>
                                                <a href="{{  url('/manan-administration2018/event-category/view-event-category/'.$eventCategory->id) }}" class="btn btn-primary btn-xs" title="details">
                                                <span class="glyphicon glyphicon-search"></span>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php $sl++ ?>
                                    </tbody>
                                @endforeach
                            </table>
                            <nav class="justify-content-end">
                                {{ $eventCategories->links() }}
                            </nav>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>

        <div class="control-sidebar-bg"></div>
    </div>

@endsection