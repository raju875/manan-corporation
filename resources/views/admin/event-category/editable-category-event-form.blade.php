@extends('admin.master')

@section('title')
    <title> Manan Corporation | Editable Event-Category Form</title>
@endsection

@section('body')

    <div class="col-md-10" style="margin:50px 0px 0px 100px">
        <h2 class="box-title text-center">Editable Event-Category Form</h2>
        <hr>
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Update Event-Category Info</h3>
                @if(Session::has('message'))
                    <h3 class="text text-center text-success">{{ Session::get('message') }}</h3>
                @endif
            </div>

            <!-- /.box-header -->
            <!-- form start -->
              <form class="form-horizontal" action="{{ url('/manan-administration2018/event-category/update-category-event')}}" method="POST" enctype="multipart/form-data" >
                    {{ csrf_field() }}
                    <?php date_default_timezone_set('Asia/Dhaka')?>
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-sm-10">
                                <input type="hidden" class="form-control" name="eventCategoryId" value="{{ $eventCategoryById->id }}" >
                            </div>
                            <span>{{ $errors->has('id') ? $errors->first('eventCategoryId') : ' ' }}</span>

                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" >Category Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="event_type" value="{{ $eventCategoryById->event_type }}" required>
                            </div>
                            <span>{{ $errors->has('event_type') ? $errors->first('event_type') : ' ' }}</span>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-2 control-label">Image</label>
                            <div class="col-sm-10">
                                <input type="file" class="form-control" name="image">
                                <img src="{{ asset($eventCategoryById->image) }}" height="90" width="110">
                                <span style="color: red">{{ $errors->has('image') ? $errors->first('image') : ' ' }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Description</label>
                            <div class="col-sm-10">
                            <textarea id="description" name="description" class="form-control" rows="50" cols="8" >
                                {{ $eventCategoryById->description}}
                    </textarea>
                                <span style="color: red">{{ $errors->has('description') ? $errors->first('description') : ' ' }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2  control-label">Publication Status</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="publication_status" required>
                                    @if($eventCategoryById->publication_status == 1 )
                                        <option value="1">Published</option>
                                        <option value="0">Unpublished</option>
                                    @else
                                        <option value="0">Unpublished</option>
                                        <option value="1">Published</option>
                                    @endif
                                </select>
                            </div>
                            <span style="color: red">{{ $errors->has('publication_status') ? $errors->first('publication_status') : ' ' }}</span>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2">
                            </div>
                            <div class="col-sm-10">
                                <button type="submit" name="btn" class="btn btn-info btn-block">Update It </button>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->

                </form>
        </div>
        <div class="control-sidebar-bg"></div>
    </div>

@endsection

@section('js')
    <script>
        CKEDITOR.replace( 'description',{
            extraPlugins: 'codesnippet'
        });
    </script>
@endsection