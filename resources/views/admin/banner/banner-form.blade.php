@extends('admin.master')

@section('title')
    <title>Manan Corporation | Banner</title>
@endsection

@section('body')
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">AboutUs</li>
            <li class="active">Add New About</li>
        </ol>
    </section>
    <div class="col-md-10" style="margin:50px 0px 0px 100px">
        <h2 class="box-title text-center">Banner</h2>
        <hr>
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Banner</h3>
                @if(Session::has('message'))
                    <h3 class="text text-center text-success">{{ Session::get('message') }}</h3>
                @endif
            </div>

            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="{{ url('/manan-administration2018/banner/add-new-banner')}}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <?php date_default_timezone_set('Asia/Dhaka')?>
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Banner Title</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="banner_title" >
                        </div>
                        <span>{{ $errors->has('banner_title') ? $errors->first('banner_title') : ' ' }}</span>

                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Banner<span style="color: red"> *</span></label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control" name="banner" required>
                            <span style="color: red">{{ $errors->has('banner') ? $errors->first('banner') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Short Description</label>
                        <div class="col-sm-10">
                            <textarea id="description" name="description" class="form-control" rows="50" cols="8">

                    </textarea>
                            <span style="color: red">{{ $errors->has('description') ? $errors->first('description') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Publication Status<span style="color: red"> *</span></label>
                        <div class="col-sm-10">
                            <select class="form-control" name="publication_status" required>
                                <option value="1">Published</option>
                                <option value="0">Unpublished</option>
                            </select>
                        </div>
                        <span style="color: red">{{ $errors->has('publication_status') ? $errors->first('publication_status') : ' ' }}</span>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2">
                        </div>
                        <div class="col-sm-10">
                            <button type="submit" name="btn" class="btn btn-info btn-block">Save it</button>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->

            </form>
        </div>
        <div class="control-sidebar-bg"></div>
    </div>

@endsection

@section('js')
    <script>
        CKEDITOR.replace( 'description',{
            extraPlugins: 'codesnippet'
        });
    </script>
@endsection
