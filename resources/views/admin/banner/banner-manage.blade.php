@extends('admin.master')

@section('title')
    <title>Manan Corporation |  Banner Table</title>
@endsection

@section('body')

    <div class="col-md-11" style="margin:50px 0px 0px 100px">
        <h2 class="box-title text-center">Manage Banner</h2>
        <hr>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Banner Table</h3>
                            @if($message = Session::get('message'))
                                <h3 class="text text-success text-center">{{ $message }}</h3>
                            @endif
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>SL No</th>
                                    <th>Banner Title</th>
                                    <th>Banner</th>
                                    <th>Short Description</th>
                                    <th>Publication Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <?php $sl=1 ?>
                                @foreach($banners as $banner )
                                    <tbody>
                                    <tr>
                                        <td>{{ $sl }}</td>
                                        <td>
                                            @if($banner->banner_title == null )
                                                {{ 'NULL' }}
                                            @else
                                            {{ $banner->banner_title }}
                                        </td>
                                        @endif
                                        <td><img src="{{ asset($banner->banner ) }}" width="150" height="180"></td>
                                        <td>
                                            @if($banner->description == null )
                                                {{ 'NULL' }}
                                            @else
                                                {{ substr(strip_tags($banner->description), 0, 50) }}
                                                <a href="{{  url('/manan-administration2018/banner/banner-view/'.$banner->id) }}">{{ strlen(strip_tags($banner->description)) > 20 ? " ...ReadMore" : "" }}</a>
                                        @endif
                                        </td>
                                        <td>
                                            @if($banner->publication_status == 1 )
                                                {{'Published'}}
                                            @else
                                                {{ 'Unpublished' }}
                                            @endif
                                        </td>

                                        <td>
                                            @if($banner->publication_status == 1)
                                                <a href="{{ url('/manan-administration2018/banner/unpublished-banner/'.$banner->id) }}" class="btn btn-success btn-xs" title="published">
                                                    <span class="glyphicon glyphicon-arrow-up"></span>
                                                </a>
                                            @else
                                                <a href="{{ url('/manan-administration2018/banner/published-banner/'.$banner->id) }}" class="btn btn-warning btn-xs" title="unpublished">
                                                    <span class="glyphicon glyphicon-arrow-down"></span>
                                                </a>
                                            @endif

                                            <a href="{{  url('/manan-administration2018/banner/editable-banner-form/'.$banner->id) }}" class="btn btn-primary btn-xs" title="edit">
                                                <span class="glyphicon glyphicon-edit"></span>
                                            </a>
                                            <a href="{{  url('/manan-administration2018/banner/delete-banner/'.$banner->id) }}" onclick="return confirm('Are you sure to delete it !!!')" class="btn btn-primary btn-xs" title="delete">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </a>
                                                <a href="{{  url('/manan-administration2018/banner/banner-view/'.$banner->id) }}" class="btn btn-primary btn-xs" title="view">
                                                <span class="glyphicon glyphicon-search"></span>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php $sl++ ?>
                                    </tbody>
                                @endforeach
                            </table>
                            <nav class="justify-content-end">
                               {{ $banners->links() }}
                            </nav>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

            <!-- /.row -->
        </section>

        <div class="control-sidebar-bg"></div>
    </div>

@endsection