@extends('admin.master')

@section('title')
    <title>Manan Corporation |  Blog Category Table</title>
@endsection

@section('body')

    <div class="col-md-12" style="margin:50px 0px 0px 40px">
        <h2 class="box-title text-center">Blog Category Table</h2>
        <hr>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title"> Blog Category Manage Table</h3>
                            @if($message = Session::get('message'))
                                <h3 class="text text-success text-center">{{ $message }}</h3>
                            @endif
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>SL No</th>
                                    <th>Blog Type</th>
                                    <th>Image</th>
                                    <th>Description</th>
                                    <th>Publication Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <?php $sl=1 ?>
                                @foreach($blogCategories as $blogCategory )
                                    <tbody>
                                    <tr>
                                        <td>{{ $sl }}</td>
                                        <td>{{ $blogCategory->blog_category }}</td>
                                        <td><img src="{{ asset( $blogCategory->image ) }}" width="100" height="80"></td>
                                        <td>
                                        @if( $blogCategory->description == null )
                                                {{ 'NULL' }}
                                            @else
                                                {{ substr(strip_tags($blogCategory->description), 0, 100) }}
                                                <a href="{{  url('/manan-administration2018/blog-category/view-blog-category/'.$blogCategory->id) }}">
                                                    {{ strlen(strip_tags($blogCategory->description)) > 40 ? " [...]ReadMore" : "" }}
                                                </a>
                                            @endif
                                        <td>
                                            @if($blogCategory->publication_status == 1)
                                                {{'Published'}}
                                            @else
                                                {{ 'Unpublished' }}
                                            @endif
                                        </td>

                                        <td>
                                            @if($blogCategory->publication_status == 1)
                                                <a href="{{ url('/manan-administration2018/blog-category/unpublished-blog-category/'.$blogCategory->id) }}" class="btn btn-success btn-xs" title="published">
                                                    <span class="glyphicon glyphicon-arrow-up"></span>
                                                </a>
                                            @else
                                                <a href="{{ url('/manan-administration2018/blog-category/published-blog-category/'.$blogCategory->id) }}" class="btn btn-warning btn-xs" title="unpublished">
                                                    <span class="glyphicon glyphicon-arrow-down"></span>
                                                </a>
                                            @endif

                                            <a href="{{  url('/manan-administration2018/blog-category/editable-blog-category-form/'.$blogCategory->id) }}" class="btn btn-primary btn-xs" title="edit">
                                                <span class="glyphicon glyphicon-edit"></span>
                                            </a>
                                            <a href="{{  url('/manan-administration2018/blog-category/delete-blog-category/'.$blogCategory->id) }}" onclick="return confirm('Are you sure to delete it !!!')" class="btn btn-primary btn-xs" title="delete">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </a>
                                                <a href="{{  url('/manan-administration2018/blog-category/view-blog-category/'.$blogCategory->id) }}" class="btn btn-primary btn-xs" title="details">
                                                <span class="glyphicon glyphicon-search"></span>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php $sl++ ?>
                                    </tbody>
                                @endforeach
                            </table>
                            <nav class="justify-content-end">
                                {{ $blogCategories->links() }}
                            </nav>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>

        <div class="control-sidebar-bg"></div>
    </div>

@endsection