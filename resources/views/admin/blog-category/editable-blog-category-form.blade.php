@extends('admin.master')

@section('title')
    <title> Manan Corporation | Editable Blog Category Form</title>
@endsection

@section('body')

    <div class="col-md-10" style="margin:50px 0px 0px 100px">
        <h2 class="box-title text-center">Editable Blog Category Form</h2>
        <hr>
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Blog Category Update</h3>
                @if(Session::has('message'))
                    <h3 class="text text-center text-success">{{ Session::get('message') }}</h3>
                @endif
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="{{ url('/manan-administration2018/blog-category/update-blog-category-info')}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                <div class="box-body">
                    <div class="form-group">
                        <div class="col-sm-10">
                            <input type="hidden" class="form-control" name="blog_category_id" value="{{ $blogCategoryById->id }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" >Blog Category</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="blog_category" value="{{ $blogCategoryById->blog_category }}" required readonly>
                        </div>
                        <span style="color: red">{{ $errors->has('blog_category') ? $errors->first('blog_category') : ' ' }}</span>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" >Image</label>
                        <div class="col-sm-10">
                            <input type="file" name="image" >
                            <br>
                            <img src="{{ asset($blogCategoryById->image) }}" height="90" width="110">
                        </div>
                        <span style="color: red">{{ $errors->has('image') ? $errors->first('image') : ' ' }}</span>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                            <textarea id="description" name="description" class="form-control" rows="50" cols="8" required>
                                {{ $blogCategoryById->description}}
                            </textarea>
                            <span style="color: red">{{ $errors->has('description') ? $errors->first('description') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-10">
                            <select class="form-control" name="publication_status" required>
                                @if($blogCategoryById->publication_status == 1 )
                                    <option value="1">Published</option>
                                    <option value="0">Unpublished</option>
                                @else
                                    <option value="0">Unpublished</option>
                                    <option value="1">Published</option>
                                @endif
                            </select>
                        </div>
                        <span style="color: red">{{ $errors->has('publication_status') ? $errors->first('publication_status') : ' ' }}</span>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2">
                        </div>
                        <div class="col-sm-10">
                            <button type="submit" name="btn" class="btn btn-info btn-block">Update It !!! </button>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </form>
        </div>
        <div class="control-sidebar-bg"></div>
    </div>

@endsection

@section('js')
    <script>
        CKEDITOR.replace( 'description',{
            extraPlugins: 'codesnippet'
        });
    </script>
@endsection