@extends('admin.master')

@section('title')
    <title>Manan Corporation | Contact </title>
@endsection

@section('body')
    <div class="col-md-11" style="margin:50px 0px 0px 100px">
        <h2 class="box-title text-center">Contact Information</h2>
        <hr>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Contact Information</h3>
                            <h3 class="text text-success text-center"></h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <table class="table table-bordered table-hover">
                                    <tr>
                                        <th class="col-sm-3">Name</th>
                                        <td class="col-sm-9">
                                            {{ $contact->name }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-3">Email</th>
                                        <td class="col-sm-9">
                                            {{ $contact->email }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-3">Submitted Date</th>
                                        <td class="col-sm-9">
                                            {{ $contact->date }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-3">Submitted Time</th>
                                        <td class="col-sm-9">
                                            {{ $contact->time }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-3"> Topics</th>
                                        <td class="col-sm-9">
                                            {{ $contact->topics }}                                        </td>
                                    </tr>
                                </table>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>

        <div class="control-sidebar-bg"></div>
    </div>

@endsection