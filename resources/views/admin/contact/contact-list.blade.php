@extends('admin.master')

@section('title')
    <title>Manan Corporation |  Contact List</title>
@endsection

@section('body')

    <div class="col-md-12" style="margin:50px 0px 0px 40px">
        <h2 class="box-title text-center">User Contact List</h2>
        <hr>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title"> Contact Manage</h3>
                            @if($message = Session::get('message'))
                                <h3 class="text text-success text-center">{{ $message }}</h3>
                            @endif
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>SL No</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Submitted Date</th>
                                    <th>Submitted Time</th>
                                    <th>Topics</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <?php $sl=1 ?>
                                @foreach($contactLists as $contactList )
                                    <tbody>
                                    <tr>
                                        <td>{{ $sl }}</td>
                                        <td>{{ $contactList->name }}</td>
                                        <td>{{ $contactList->email }}</td>
                                        <td>{{ $contactList->date }}</td>
                                        <td>{{ $contactList->time }}</td>
                                        <td>{{ substr(strip_tags($contactList->topics), 0, 100) }}
                                            <a href="{{ url('/manan-administration2018/contact/view-contact-details/'.$contactList->id) }}">{{ strlen(strip_tags($contactList->topics)) > 40 ? " [...]ReadMore" : "" }}</a>
                                        </td>
                                        <td>
                                            <a href="{{  url('/manan-administration2018/contact/delete-contact/'.$contactList->id) }}" onclick="return confirm('Are you sure to delete it !!!')" class="btn btn-primary btn-xs" title="delete">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </a>
                                            <a href="{{  url('/manan-administration2018/contact/view-contact-details/'.$contactList->id) }}" class="btn btn-primary btn-xs" title="details">
                                                <span class="glyphicon glyphicon-search"></span>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php $sl++ ?>
                                    </tbody>
                                @endforeach
                            </table>
                            <nav class="justify-content-end">
                                {{ $contactLists->links() }}
                            </nav>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>

        <div class="control-sidebar-bg"></div>
    </div>

@endsection