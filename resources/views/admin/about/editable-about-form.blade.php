@extends('admin.master')

@section('title')
    <title> Manan Corporation | Editable About Form</title>
    @endsection

@section('body')

    <div class="col-md-10" style="margin:50px 0px 0px 100px">
        <h2 class="box-title text-center">AboutUs</h2>
        <hr>
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">AboutUs</h3>
                @if(Session::has('message'))
                    <h3 class="text text-center text-success">{{ Session::get('message') }}</h3>
                @endif
            </div>

            <!-- /.box-header -->
            <!-- form start -->

            @foreach($aboutInfos as $aboutInfo)

            <form class="form-horizontal" action="{{ url('/manan-administration2018/about/update-about')}}" method="POST" enctype="multipart/form-data" >
                {{ csrf_field() }}
                <?php date_default_timezone_set('Asia/Dhaka')?>
                <div class="box-body">
                    <div class="form-group">
                        <div class="col-sm-10">
                            <input type="hidden" class="form-control" name="id" value="{{ $aboutInfo->id }}">
                        </div>
                        <span>{{ $errors->has('title') ? $errors->first('title') : ' ' }}</span>

                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" >Title</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="title" value="{{ $aboutInfo->title }}" readonly>
                        </div>
                        <span>{{ $errors->has('title') ? $errors->first('title') : ' ' }}</span>

                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                            <textarea id="description" name="description" class="form-control" rows="50" cols="8" required>
                                {{ $aboutInfo->description}}
                    </textarea>
                            <span style="color: red">{{ $errors->has('description') ? $errors->first('description') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-10">
                            <select class="form-control" name="publication_status" required>
                                @if($aboutInfo->publication_status == 1 )
                                <option value="1">Published</option>
                                <option value="0">Unpublished</option>
                                    @else
                                    <option value="0">Unpublished</option>
                                    <option value="1">Published</option>
                                    @endif
                            </select>
                        </div>
                        <span style="color: red">{{ $errors->has('publication_status') ? $errors->first('publication_status') : ' ' }}</span>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2">
                        </div>
                        <div class="col-sm-10">
                            <button type="submit" name="btn" class="btn btn-info btn-block">Update It </button>
                        </div>
                    </div>
                    @endforeach
                </div>
                <!-- /.box-body -->

            </form>
        </div>
        <div class="control-sidebar-bg"></div>
    </div>

    @endsection

@section('js')
    <script>
        CKEDITOR.replace( 'description',{
            extraPlugins: 'codesnippet'
        });
    </script>
@endsection