@extends('admin.master')

@section('title')
    <title>Manan Corporation | AboutUs</title>
    @endsection

@section('body')
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">AboutUs</li>
            <li class="active">Add New About</li>
        </ol>
    </section>
    <div class="col-md-10" style="margin:50px 0px 0px 100px">
        <h2 class="box-title text-center">AboutUs</h2>
        <hr>
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">AboutUs</h3>
                @if(Session::has('message'))
                    <h3 class="text text-center text-success">{{ Session::get('message') }}</h3>
                @endif
            </div>

            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="{{ url('/manan-administration2018/about/add-new-about')}}" method="POST" enctype="multipart/form-data" >
                {{ csrf_field() }}
                <?php date_default_timezone_set('Asia/Dhaka')?>
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" >Title</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="title" value="AboutUs" readonly>
                        </div>
                        <span>{{ $errors->has('title') ? $errors->first('title') : ' ' }}</span>

                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">About<span style="color: red"> *</span></label>
                        <div class="col-sm-10">
                           <textarea name="description" class="form-control" id="description" required></textarea>
                            <span style="color: red">{{ $errors->has('description') ? $errors->first('description') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Publication Status<span style="color: red"> *</span></label>
                        <div class="col-sm-10">
                        <select class="form-control" name="publication_status" required>
                            <option value="1">Published</option>
                            <option value="0">Unpublished</option>
                        </select>
                        </div>
                        <span style="color: red">{{ $errors->has('publication_status') ? $errors->first('publication_status') : ' ' }}</span>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2">
                        </div>
                        <div class="col-sm-10">
                            <button type="submit" name="btn" class="btn btn-info btn-block">Save </button>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->

            </form>
        </div>
        <div class="control-sidebar-bg"></div>
    </div>

    {{--<script src="{{ asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>--}}
    {{--<script src="{{ asset('/vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>--}}
    {{--<script>--}}
        {{--$('description').ckeditor();--}}
        {{--// $('.description').ckeditor(); // if class is prefered.--}}
    {{--</script>--}}


@endsection

@section('js')
    <script>
        CKEDITOR.replace( 'description',{
            extraPlugins: 'codesnippet'
        });
    </script>
    @endsection
