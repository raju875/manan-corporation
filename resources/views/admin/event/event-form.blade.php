@extends('admin.master')

@section('title')
    <title>Manan Corporation | Event Form</title>
@endsection

@section('body')
    <div class="col-md-10" style="margin:50px 0px 0px 100px">
        <h2 class="box-title text-center">Event Form</h2>
        <hr>
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Create New Event</h3>
                @if(Session::has('message'))
                    <h3 class="text text-center text-success">{{ Session::get('message') }}</h3>
                @endif
                @if(Session::has('alert'))
                    <h3 class="text text-center text-danger">{{ Session::get('alert') }}</h3>
                @endif
            </div>

            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="{{ url('/manan-administration2018/event/create-new-event')}}" method="POST" enctype="multipart/form-data" >
                {{ csrf_field() }}
                <div class="box-body">
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Event Name<span style="color: red"> *</span></label>
                         <div class="col-sm-10">
                            <input type="text" class="form-control" name="event_name" placeholder="Event Name" >
                            <span style="color: red">{{ $errors->has('event_name') ? $errors->first('event_name') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Event Type<span style="color: red"> *</span></label>
                        <div class="col-sm-10">
                            <select class="form-control" name="event_category_id" required>
                                @foreach($categories as $category )
                                    <option value="{{ $category->id }}">{{ $category->event_type }}</option>
                                @endforeach
                            </select>
                            <span style="color: red">{{ $errors->has('event_category_id') ? $errors->first('event_category_id') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Location<span style="color: red"> *</span></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="location" placeholder="Location" >
                            <span style="color: red">{{ $errors->has('location') ? $errors->first('location') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Start Date<span style="color: red"> *</span></label>
                        <div class="col-sm-3">
                            <?php date_default_timezone_set('Asia/Dhaka')?>
                                <input type="date" id="start_date" name="start_date"  min="{{ date("Y-m-d") }}"   required>
                            <span style="color: red">{{ $errors->has('start_date') ? $errors->first('start_date') : ' ' }}</span>
                        </div>
                        <div class="col-sm-7">
                            <input type="time" name="start_time" required>
                            <span style="color: red">{{ $errors->has('start_time') ? $errors->first('start_time') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">End Date<span style="color: red"> *</span></label>
                        <div class="col-sm-3">
                            <input type="date" id="end_date" name="end_date" min="{{ date("Y-m-d") }}" required>
                            <span style="color: red">{{ $errors->has('end_date') ? $errors->first('end_date') : ' ' }}</span>
                        </div>
                        <div class="col-sm-7">
                            <input type="time" name="end_time" required>
                            <span style="color: red">{{ $errors->has('end_time') ? $errors->first('end_time') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Description<span style="color: red"> *</span></label>
                        <div class="col-sm-10">
                            <textarea id="description" name="description" class="form-control" rows="50" cols="8" placeholder="Tell people more about the event ... ..." >
                            </textarea>
                            <span style="color: red">{{ $errors->has('description') ? $errors->first('description') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Photo<span style="color: red"> *</span></label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control" name="photo" >
                            <span style="color: red">{{ $errors->has('photo') ? $errors->first('photo') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2  control-label">Publication Status<span style="color: red"> *</span></label>
                        <div class="col-sm-10">
                            <select class="form-control" name="publication_status" >
                                <option value="1">Published</option>
                                <option value="0">Unpublished</option>
                            </select>
                            <span style="color: red">{{ $errors->has('publication_status') ? $errors->first('publication_status') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2">
                        </div>
                        <div class="col-sm-10">
                            <button type="submit" name="btn" class="btn btn-info btn-block">Create Event</button>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->

            </form>
        </div>
        <div class="control-sidebar-bg"></div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

@endsection

@section('js')
    <script>
        CKEDITOR.replace( 'description',{
            extraPlugins: 'codesnippet'
        });
    </script>
@endsection