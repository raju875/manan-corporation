@extends('admin.master')

@section('title')
    <title>Manan Corporation |  Event Table</title>
@endsection

@section('body')

    <div class="col-md-12" style="margin:50px 0px 0px 40px">
        <h2 class="box-title text-center">Event Table</h2>
        <hr>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title"> Event Manage Table</h3>
                            @if($message = Session::get('message'))
                                <h3 class="text text-success text-center">{{ $message }}</h3>
                            @endif
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>SL No</th>
                                    <th>Event Name</th>
                                    <th>Event Type</th>
                                    <th>Location</th>
                                    <th>Starting Date</th>
                                    <th>Starting Time</th>
                                    <th>Ending Date</th>
                                    <th>Ending Time</th>
                                    <th>Description</th>
                                    <th>Photo</th>
                                    <th>Publication Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <?php $sl=1 ?>
                                @foreach($events as $event )
                                    <tbody>
                                    <tr>
                                        <td>{{ $sl }}</td>
                                        <td>{{ $event->event_name }}</td>
                                        <td>{{ $event->event_type }}</td>
                                        <td>{{ $event->location }}</td>
                                        <td>{{ $event->start_date }}</td>
                                        <td>{{ $event->start_time }}</td>
                                        <td>{{ $event->end_date }}</td>
                                        <td>{{ $event->end_time }}</td>
                                        <td>{{ substr(strip_tags($event->description), 0, 100) }}
                                            <a href="{{  url('/manan-administration2018/event/view-event/'.$event->id) }}">
                                                {{ strlen(strip_tags($event->description)) > 30 ? " ...ReadMore" : "" }}
                                            </a>
                                        </td>
                                        <td><img src="{{ asset($event->photo ) }}" width="80" height="100"></td>
                                        <td>
                                            @if($event->publication_status == 1)
                                                {{'Published'}}
                                            @else
                                                {{ 'Unpublished' }}
                                            @endif
                                        </td>

                                        <td>
                                            @if($event->publication_status == 1)
                                                <a href="{{ url('/manan-administration2018/event/unpublished-event/'.$event->id) }}" class="btn btn-success btn-xs" title="published">
                                                    <span class="glyphicon glyphicon-arrow-up"></span>
                                                </a>
                                            @else
                                                <a href="{{ url('/manan-administration2018/event/published-event/'.$event->id) }}" class="btn btn-warning btn-xs" title="unpublished">
                                                    <span class="glyphicon glyphicon-arrow-down"></span>
                                                </a>
                                            @endif

                                            <a href="{{  url('/manan-administration2018/event/editable-event-form/'.$event->id) }}" class="btn btn-primary btn-xs" title="edit">
                                                <span class="glyphicon glyphicon-edit"></span>
                                            </a>
                                            <a href="{{  url('/manan-administration2018/event/delete-event/'.$event->id) }}" onclick="return confirm('Are you sure to delete it !!!')" class="btn btn-primary btn-xs" title="delete">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </a>
                                                <a href="{{  url('/manan-administration2018/event/view-event/'.$event->id) }}"  class="btn btn-primary btn-xs" title="details">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php $sl++ ?>
                                    </tbody>
                                @endforeach
                            </table>
                            <nav class="justify-content-end">
                                {{ $events->links() }}
                            </nav>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>

        <div class="control-sidebar-bg"></div>
    </div>

@endsection