@extends('admin.master')

@section('title')
    <title> Manan Corporation | Editable Event Form</title>
@endsection

@section('body')

    <div class="col-md-10" style="margin:50px 0px 0px 100px">
        <h2 class="box-title text-center">Editable Event Form</h2>
        <hr>
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Update Even Info</h3>
                @if(Session::has('message'))
                    <h3 class="text text-center text-success">{{ Session::get('message') }}</h3>
                @endif
                @if(Session::has('aleart'))
                    <h3 class="text text-center text-danger">{{ Session::get('aleart') }}</h3>
                @endif
            </div>

            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="{{ url('/manan-administration2018/event/update-event')}}" method="POST" enctype="multipart/form-data" >
                {{ csrf_field() }}
                <div class="box-body">
                    <div class="form-group">
                        <div class="col-sm-10">
                            <input type="hidden" class="form-control" name="event_id" value="{{ $eventById->id }}" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Event Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="event_name" value="{{ $eventById->event_name }}" >
                            <span style="color: red">{{ $errors->has('event_name') ? $errors->first('event_name') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Event Type</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="event_category_id" value="{{ $eventCategoryById->event_type }}" required  readonly>
                            <span style="color: red">{{ $errors->has('event_category_id') ? $errors->first('event_category_id') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Location</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="location" value="{{ $eventById->location }}" >
                            <span style="color: red">{{ $errors->has('location') ? $errors->first('location') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Start Date</label>
                        <div class="col-sm-3">
                            <?php date_default_timezone_set('Asia/Dhaka')?>
                            <input type="date" id="start_date" name="start_date" min="{{ date("Y-m-d") }}" value="{{ $eventById->start_date }}"  required>
                            <span style="color: red">{{ $errors->has('start_date') ? $errors->first('start_date') : ' ' }}</span>
                        </div>
                        <div class="col-sm-7">
                            <input type="time" name="start_time" value="{{ $eventById->start_time }}" required>
                            <span style="color: red">{{ $errors->has('start_time') ? $errors->first('start_time') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">End Date</label>
                        <div class="col-sm-3">
                            <input type="date" id="end_date" name="end_date" min="{{ date("Y-m-d") }}" value="{{ $eventById->end_date }}"  required>
                            <span style="color: red">{{ $errors->has('end_date') ? $errors->first('end_date') : ' ' }}</span>
                        </div>
                        <div class="col-sm-7">
                            <input type="time" name="end_time"  value="{{ $eventById->end_time }}" required>
                            <span style="color: red">{{ $errors->has('end_time') ? $errors->first('end_time') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                            <textarea id="description" name="description" class="form-control" rows="50" cols="8" placeholder="Tell people more about the event ... ..." >
                                {{ $eventById->description }}
                            </textarea>
                            <span style="color: red">{{ $errors->has('description') ? $errors->first('description') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Photo</label>
                        <div class="col-sm-10">
                            <input type="file" id="image" name="photo"  accept="image/*"/><span style="color: red">max size : 1.2MB</span><br>
                            <img src="{{ asset($eventById->photo) }}" height="80" width="80">
                            <span style="color: red">{{ $errors->has('photo') ? $errors->first('photo') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2  control-label">Publication Status</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="publication_status" >
                                @if($eventById->publication_status == 1 )
                                    <option value="1">Published</option>
                                    <option value="0">Unpublished</option>
                                @else
                                    <option value="0">Unpublished</option>
                                    <option value="1">Published</option>
                                @endif
                            </select>
                            <span style="color: red">{{ $errors->has('publication_status') ? $errors->first('publication_status') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2">
                        </div>
                        <div class="col-sm-10">
                            <button type="submit" name="btn" class="btn btn-info btn-block">Update Event</button>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->

            </form>
        </div>
        <div class="control-sidebar-bg"></div>
    </div>

@endsection

@section('js')
    <script>
        CKEDITOR.replace( 'description',{
            extraPlugins: 'codesnippet'
        });
    </script>
@endsection