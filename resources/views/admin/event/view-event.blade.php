@extends('admin.master')

@section('title')
    <title>Manan Corporation | Event </title>
@endsection

@section('body')
    <div class="col-md-11" style="margin:50px 0px 0px 100px">
        <h2 class="box-title text-center">Event Information</h2>
        <hr>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Event Information</h3>
                            <h3 class="text text-success text-center"></h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <table class="table table-bordered table-hover">
                                    <tr>
                                        <th class="col-sm-3"> Image</th>
                                        <td class="col-sm-9">
                                            <img src="{{ asset( $eventById->photo ) }}" height="200" width="225">
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-3">Event Information</th>
                                        <td class="col-sm-9">
                                            {{ $eventById->event_name }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-3">Event Location</th>
                                        <td class="col-sm-9">
                                            {{ $eventById->location }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-3">Starting Date</th>
                                        <td class="col-sm-9">
                                            {{ $eventById->start_date }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-3">Starting Time</th>
                                        <td class="col-sm-9">
                                           {{ $eventById->start_time }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-3">Ending Date</th>
                                        <td class="col-sm-9">
                                           {{ $eventById->end_date }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-3">Ending Time</th>
                                        <td class="col-sm-9">
                                           {{ $eventById->end_time }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-3">Event Location</th>
                                        <td class="col-sm-9">
                                            {{ $eventById->location }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-3">Description</th>
                                        <td class="col-sm-9">
                                            <?php echo $eventById->description ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-3">Publication Status</th>
                                        <td class="col-sm-9">
                                            @if($eventById->publication_status == 1)
                                                {{'Published'}}
                                            @else
                                                {{ 'Unpublished' }}
                                            @endif
                                        </td>
                                    </tr>
                                </table>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>

        <div class="control-sidebar-bg"></div>
    </div>

@endsection