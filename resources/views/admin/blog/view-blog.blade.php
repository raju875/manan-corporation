@extends('admin.master')

@section('title')
    <title>Manan Corporation | Blog </title>
@endsection

@section('body')
    <div class="col-md-11" style="margin:50px 0px 0px 100px">
        <h2 class="box-title text-center">Blog Information</h2>
        <hr>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Blog Information</h3>
                            <h3 class="text text-success text-center"></h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <table class="table table-bordered table-hover">
                                    <tr>
                                        <th class="col-sm-3">Blog Title</th>
                                        <td class="col-sm-9">
                                            {{ $blogById->blog_title }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-3">Blog Category</th>
                                        <td class="col-sm-9">
                                            {{ $blogCategoryById->blog_category }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-3">Blog Writer</th>
                                        <td class="col-sm-9">
                                            {{ $blogById->writer }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-3">Submitted Date</th>
                                        <td class="col-sm-9">
                                            {{ $blogById->submitted_date }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-3"> Image</th>
                                        <td class="col-sm-9">
                                           <img src="{{ asset( $blogById->image ) }}" height="200" width="225">
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-3">Blog Content</th>
                                        <td class="col-sm-9">
                                                <?php echo $blogById->description ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-3">Publication Status</th>
                                        <td class="col-sm-9">
                                            @if($blogById->publication_status == 1)
                                                {{'Published'}}
                                            @else
                                                {{ 'Unpublished' }}
                                            @endif
                                        </td>
                                    </tr>
                                </table>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>

        <div class="control-sidebar-bg"></div>
    </div>

@endsection