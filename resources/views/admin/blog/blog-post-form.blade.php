@extends('admin.master')

@section('title')
    <title>Manan Corporation | Blog Post Form</title>
@endsection

@section('body')

    <div class="col-md-10" style="margin:50px 0px 0px 100px">
        <h2 class="box-title text-center">Blog Post Form</h2>
        <hr>
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title"> Blog Post</h3>
                @if(Session::has('message'))
                    <h3 class="text text-center text-success">{{ Session::get('message') }}</h3>
                @endif
                @if(Session::has('aleart'))
                    <h3 class="text text-center text-danger">{{ Session::get('aleart') }}</h3>
                @endif
            </div>

            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="{{ url('/manan-administration2018/blog/blog-post' )}}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="box-body">
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Blog Title<span style="color: red"> *</span></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="blog_title" placeholder="Blog Title" required >
                            <span style="color: red">{{ $errors->has('blog_title') ? $errors->first('blog_title') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Blog Category<span style="color: red"> *</span></label>
                        <div class="col-sm-10">
                            <select class="form-control" name="blog_category_id" required>
                                @foreach($publishedBlogCategories as $publishedBlogCategory )
                                    <option value="{{ $publishedBlogCategory->id }}">{{ $publishedBlogCategory->blog_category }}</option>
                                @endforeach
                            </select>
                            <span style="color: red">{{ $errors->has('blog_category_id') ? $errors->first('blog_category_id') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Writer<span style="color: red"> *</span></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="writer" placeholder="Writer of this blog" required>
                            <span style="color: red">{{ $errors->has('writer') ? $errors->first('writer') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Submitted Date<span style="color: red"> *</span></label>
                        <div class="col-sm-10">
                            <?php date_default_timezone_set('Asia/Dhaka')?>
                            <input type="date" id="submitted_date" name="submitted_date"  value="{{ date("Y-m-d") }}"   readonly>
                            <span style="color: red">{{ $errors->has('submitted_date') ? $errors->first('submitted_date') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Image<span style="color: red"> *</span></label>
                        <div class="col-sm-10">
                            <input type="file" name="image" required >
                            <span style="color: red">{{ $errors->has('image') ? $errors->first('image') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Content<span style="color: red"> *</span> </label>
                        <div class="col-sm-10">
                            <textarea  name="description" class="form-control" rows="50" cols="8" required placeholder="Tell people more about the event ... ..." >
                            </textarea>
                            <span style="color: red">{{ $errors->has('description') ? $errors->first('description') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2  control-label">Publication Status<span style="color: red"> *</span></label>
                        <div class="col-sm-10">
                            <select class="form-control" name="publication_status" required>
                                <option value="1">Published</option>
                                <option value="0">Unpublished</option>
                            </select>
                            <span style="color: red">{{ $errors->has('publication_status') ? $errors->first('publication_status') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2">
                        </div>
                        <div class="col-sm-10">
                            <button type="submit" name="btn" class="btn btn-info btn-block"> Blog Post</button>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->

            </form>
        </div>
        <div class="control-sidebar-bg"></div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

@endsection


@section('js')
    <script>
        CKEDITOR.replace( 'description',{
            extraPlugins: 'codesnippet'
        });
    </script>
@endsection