@extends('admin.master')

@section('title')
    <title>Manan Corporation | Edite Blog </title>
@endsection

@section('body')

    <div class="col-md-10" style="margin:50px 0px 0px 100px">
        <h2 class="box-title text-center"> Editable Blog Form</h2>
        <hr>
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Edite Blog </h3>
                @if(Session::has('message'))
                    <h3 class="text text-center text-success">{{ Session::get('message') }}</h3>
                @endif
                @if(Session::has('aleart'))
                    <h3 class="text text-center text-danger">{{ Session::get('aleart') }}</h3>
                @endif
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="{{ url('/manan-administration2018/blog/update-blog' )}}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="box-body">
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Blog Title</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="blog_title" value="{{ $blogById->blog_title }}" required >
                            <span style="color: red">{{ $errors->has('blog_title') ? $errors->first('blog_title') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-10">
                            <input type="hidden" class="form-control" name="blog_id" value="{{ $blogById->id }}" required >
                            <span style="color: red">{{ $errors->has('blog_title') ? $errors->first('blog_title') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Blog Category</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="blog_category_id" >
                                <option value="{{ $blogCategoryById->id }}">{{ $blogCategoryById->blog_category }}</option>
                            </select>
                            <span style="color: red">{{ $errors->has('blog_category_id') ? $errors->first('blog_category_id') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Writer</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="writer" value="{{ $blogById->writer }}" required>
                            <span style="color: red">{{ $errors->has('writer') ? $errors->first('writer') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Submitted Date</label>
                        <div class="col-sm-10">
                            <?php date_default_timezone_set('Asia/Dhaka')?>
                            <input type="date" id="submitted_date" name="submitted_date"  value="{{ date("Y-m-d") }}"   readonly>
                            <span style="color: red">{{ $errors->has('submitted_date') ? $errors->first('submitted_date') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Image</label>
                        <div class="col-sm-10">
                            <input type="file" name="image">
                            <img src="{{ asset($blogById->image) }}" height="90" width="110">
                            <span style="color: red">{{ $errors->has('image') ? $errors->first('image') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Content</label>
                        <div class="col-sm-10">
                            <textarea  name="description" class="form-control" rows="50" cols="8" required placeholder="Tell people more about the event ... ..." >
                                {{ $blogById->description }}
                            </textarea>
                            <span style="color: red">{{ $errors->has('description') ? $errors->first('description') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2  control-label">Publication Status</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="publication_status" required>
                                @if($blogById->publication_status == 1 )
                                    <option value="1">Published</option>
                                    <option value="0">Unpublished</option>
                                @else
                                    <option value="0">Unpublished</option>
                                    <option value="1">Published</option>
                                @endif
                            </select>
                            <span style="color: red">{{ $errors->has('publication_status') ? $errors->first('publication_status') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2">
                        </div>
                        <div class="col-sm-10">
                            <button type="submit" name="btn" class="btn btn-info btn-block"> Update Blog !!!</button>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->

            </form>
        </div>
        <div class="control-sidebar-bg"></div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

@endsection


@section('js')
    <script>
        CKEDITOR.replace( 'description',{
            extraPlugins: 'codesnippet'
        });
    </script>
@endsection