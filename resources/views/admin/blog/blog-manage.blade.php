@extends('admin.master')

@section('title')
    <title>Manan Corporation |  Blog Table</title>
@endsection

@section('body')

    <div class="col-md-12" style="margin:50px 0px 0px 40px">
        <h2 class="box-title text-center">Blog Table</h2>
        <hr>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title"> Blog Manage Table</h3>
                            @if($message = Session::get('message'))
                                <h3 class="text text-success text-center">{{ $message }}</h3>
                            @endif
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>SL No</th>
                                    <th>Blog Title</th>
                                    <th>Blog Category</th>
                                    <th>Writer</th>
                                    <th>Submitted Date</th>
                                    <th>Blog Image</th>
                                    <th>Content</th>
                                    <th>Publication Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <?php $sl=1 ?>
                                @foreach($blogs as $blog )
                                    <tbody>
                                    <tr>
                                        <td>{{ $sl }}</td>
                                        <td>{{ $blog->blog_title }}</td>
                                        <td>{{ $blog->blog_category }}</td>
                                        <td>{{ $blog->writer }}</td>
                                        <td>{{ $blog->submitted_date }}</td>
                                        <td><img src="{{ asset($blog->image ) }}" width="80" height="100"></td>
                                        <td>{{ substr(strip_tags($blog->description), 0, 100) }}
                                            <a href="{{  url('/manan-administration2018/blog/view-blog/'.$blog->id) }}">{{ strlen(strip_tags($blog->description)) > 40 ? " ...ReadMore" : "" }}</a>
                                        </td>
                                        <td>
                                            @if($blog->publication_status == 1 )
                                                {{'Published'}}
                                            @else
                                                {{ 'Unpublished' }}
                                            @endif
                                        </td>

                                        <td>
                                            @if($blog->publication_status == 1 )
                                                <a href="{{ url('/manan-administration2018/blog/unpublished-blog/'.$blog->id) }}" class="btn btn-success btn-xs" title="published">
                                                    <span class="glyphicon glyphicon-arrow-up"></span>
                                                </a>
                                            @else
                                                <a href="{{ url('/manan-administration2018/blog/published-blog/'.$blog->id) }}" class="btn btn-warning btn-xs" title="unpublished">
                                                    <span class="glyphicon glyphicon-arrow-down"></span>
                                                </a>
                                            @endif

                                            <a href="{{  url('/manan-administration2018/blog/editable-blog-form/'.$blog->id) }}" class="btn btn-primary btn-xs" title="edit">
                                                <span class="glyphicon glyphicon-edit"></span>
                                            </a>
                                            <a href="{{  url('/manan-administration2018/blog/delete-blog/'.$blog->id) }}" onclick="return confirm('Are you sure to delete it !!!')" class="btn btn-primary btn-xs" title="delete">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </a>
                                                <a href="{{  url('/manan-administration2018/blog/view-blog/'.$blog->id) }}" class="btn btn-primary btn-xs" title="view">
                                                <span class="glyphicon glyphicon-search"></span>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php $sl++ ?>
                                    </tbody>
                                @endforeach
                            </table>
                            <nav class="justify-content-end">
                                {{ $blogs->links() }}
                            </nav>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

            <!-- /.row -->
        </section>

        <div class="control-sidebar-bg"></div>
    </div>

@endsection