@extends('admin.master')

@section('body')

        <div class="container">
            <div class="row">
                <div class="col-md-10 "style="margin: 100px" >
                    <img src="{{ asset('admin/loginProfileImages/') }}/{{ $user->avatar }}" style="width:150px; height: 150px; float: left; border-radius: 50%; margin-right: 25px "/>
                    <h2>{{ $user->name }}'s Profile</h2>
                    <form enctype="multipart/form-data" action="{{ url('/manan-administration2018/update-profile') }}" method="POST">
                        {{ csrf_field() }}
                        <label>Update Profile Picture</label>
                        <input type="file" name="avatar">
                        <input type="submit" class="pull-right btb btn-sm btn-primary">
                    </form>
                </div>
            </div>
        </div>
    @endsection