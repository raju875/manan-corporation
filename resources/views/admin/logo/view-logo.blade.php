@extends('admin.master')

@section('title')
    <title>Manan Corporation | Logo </title>
@endsection

@section('body')
    <div class="col-md-11" style="margin:50px 0px 0px 100px">
        <h2 class="box-title text-center">Logo Information</h2>
        <hr>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Logo Information</h3>
                            <h3 class="text text-success text-center"></h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <table class="table table-bordered table-hover">
                                    <tr>
                                        <th class="col-sm-3">Logo Title</th>
                                        <td class="col-sm-9">
                                            @if( $logoById->logo_title == null )
                                                {{'NULL'}}
                                            @else
                                                {{ $logoById->logo_title }}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-3">Logo</th>
                                        <td class="col-sm-9">
                                            <img src="{{ asset($logoById->logo ) }}" height="250" width="330">
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-3">\Description</th>
                                        <td class="col-sm-9">
                                            @if( $logoById->description == null )
                                                {{'NULL'}}
                                            @else
                                                <?php echo $logoById->description ?>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-3">Publication Status</th>
                                        <td class="col-sm-9">
                                            @if($logoById->publication_status == 1)
                                                {{'Published'}}
                                            @else
                                                {{ 'Unpublished' }}
                                            @endif
                                        </td>
                                    </tr>
                                </table>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>

        <div class="control-sidebar-bg"></div>
    </div>

@endsection