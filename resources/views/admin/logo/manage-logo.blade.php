@extends('admin.master')

@section('title')
    <title>Manan Corporation |  Logo Table</title>
@endsection

@section('body')

    <div class="col-md-11" style="margin:50px 0px 0px 100px">
        <h2 class="box-title text-center">Manage Logo</h2>
        <hr>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Logo Manage Table</h3>
                            @if(Session::has('message'))
                                <h3 class="text text-center text-success">{{ Session::get('message') }}</h3>
                            @endif
                            @if(Session::has('alert'))
                                <h3 class="text text-center text-danger">{{ Session::get('alert') }}</h3>
                            @endif
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>SL No</th>
                                    <th>Logo Title</th>
                                    <th>Logo</th>
                                    <th>Description</th>
                                    <th>Publication Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <?php $sl=1 ?>
                                @foreach($logos as $logo )
                                    <tbody>
                                    <tr>
                                        <td>{{ $sl }}</td>
                                        <td>
                                            @if($logo->logo_title == null )
                                                {{ 'NULL' }}
                                            @else
                                                {{ $logo->logo_title }}
                                        </td>
                                        @endif
                                        <td><img src="{{ asset($logo->logo ) }}" width="170" height="130"></td>
                                        <td>
                                            @if($logo->description == null )
                                                {{ 'NULL' }}
                                            @else
                                                {{ substr(strip_tags($logo->description), 0, 50) }}
                                                <a href="{{  url('/manan-administration2018/logo/logo-view/'.$logo->id) }}">
                                                    {{ strlen(strip_tags($logo->description)) > 20 ? " [...]ReadMore" : "" }}
                                                </a>
                                            @endif
                                        </td>
                                        <td>
                                            @if($logo->publication_status == 1 )
                                                {{'Published'}}
                                            @else
                                                {{ 'Unpublished' }}
                                            @endif
                                        </td>

                                        <td>
                                            @if($logo->publication_status == 1)
                                                <a href="{{ url('/manan-administration2018/logo/unpublished-logo/'.$logo->id) }}" class="btn btn-success btn-xs" title="published">
                                                    <span class="glyphicon glyphicon-arrow-up"></span>
                                                </a>
                                            @else
                                                <a href="{{ url('/manan-administration2018/logo/published-logo/'.$logo->id) }}" class="btn btn-warning btn-xs" title="unpublished">
                                                    <span class="glyphicon glyphicon-arrow-down"></span>
                                                </a>
                                            @endif

                                            <a href="{{  url('/manan-administration2018/logo/editable-logo-form/'.$logo->id) }}" class="btn btn-primary btn-xs" title="edit">
                                                <span class="glyphicon glyphicon-edit"></span>
                                            </a>
                                            <a href="{{  url('/manan-administration2018/logo/delete-logo/'.$logo->id) }}" onclick="return confirm('Are you sure to delete it !!!')" class="btn btn-primary btn-xs" title="delete">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </a>
                                            <a href="{{  url('/manan-administration2018/logo/logo-view/'.$logo->id) }}" class="btn btn-primary btn-xs" title="view">
                                                <span class="glyphicon glyphicon-search"></span>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php $sl++ ?>
                                    </tbody>
                                @endforeach
                            </table>
                            <nav class="justify-content-end">
                                {{ $logos->links() }}
                            </nav>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>

            <!-- /.row -->
        </section>

        <div class="control-sidebar-bg"></div>
    </div>

@endsection