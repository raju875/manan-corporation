@extends('admin.master')

@section('title')
    <title>Manan Corporation | Project View </title>
@endsection

@section('body')
    <div class="col-md-11" style="margin:50px 0px 0px 100px">
        <h2 class="box-title text-center">Project Information</h2>
        <hr>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Project Information</h3>
                            <h3 class="text text-success text-center"></h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <table class="table table-bordered table-hover">
                                    <tr>
                                        <th class="col-sm-3">Project Name</th>
                                        <td class="col-sm-9">{{ $projectById->project_name }}</td>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-3">Project Image</th>
                                        <td class="col-sm-9"> <img src="{{ asset($projectById->image) }}" height="200" width="250"></td>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-3">Project Sub Image</th>
                                        @foreach($subImageById as $subImageById )
                                        <tr>
                                            <th class="col-sm-3"></th>
                                           <td class="col-sm-9"> <img src="{{ asset($subImageById->sub_image) }}" height="200" width="250"></td>
                                       </tr>
                                        @endforeach
                                    </tr>
                                        <tr>
                                        <th class="col-sm-3">Project Description </th>
                                            <td class="col-sm-9"><?php echo $projectById->description ?> </td>
                                       </tr>
                                    </tr>
                                    </tr>
                                    <tr>
                                        <th class="col-sm-3">Publication Status</th>
                                        <td class="col-sm-9">
                                            @if($projectById->publication_status == 1 )
                                                {{'Published'}}
                                            @else
                                                {{ 'Unpublished' }}
                                            @endif
                                        </td>
                                    </tr>
                                </table>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>

        <div class="control-sidebar-bg"></div>
    </div>

@endsection