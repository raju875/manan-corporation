@extends('admin.master')

@section('title')
    <title> Manan Corporation | Editable Project Form</title>
@endsection

@section('body')

    <div class="col-md-10" style="margin:50px 0px 0px 100px">
        <h2 class="box-title text-center">Editable Project Form</h2>
        <hr>
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Update Project Info</h3>
                @if(Session::has('message'))
                    <h3 class="text text-center text-success">{{ Session::get('message') }}</h3>
                @endif
                @if(Session::has('aleart'))
                    <h3 class="text text-center text-danger">{{ Session::get('aleart') }}</h3>
                @endif
            </div>

            <!-- /.box-header -->
            <!-- form start -->
                <form class="form-horizontal" action="{{ url('/manan-administration2018/project/update-project')}}" method="POST" enctype="multipart/form-data" >
                    {{ csrf_field() }}
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-sm-10">
                                <input type="hidden" name="project_id" class="form-control" value="{{ $projectById->id }}" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-2 control-label">Project Name<span style="color: red"> *</span></label>
                            <div class="col-sm-10">
                                <input type="text" name="project_name" class="form-control" value="{{ $projectById->project_name }}"  placeholder="Project Name">
                                <span style="color: red">{{ $errors->has('project_name') ? $errors->first('project_name') : ' ' }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2  control-label">Category Name<span style="color: red"> *</span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="category_name" class="form-control" value="{{ $categoryById->category_name }}"  readonly>
                                    <span style="color: red">{{ $errors->has('category_name') ? $errors->first('category_name') : ' ' }}</span>
                                </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Image</label>
                            <div class="col-sm-10">
                                <input type="file" id="image" name="image"  accept="image/*"/><span style="color: red">max size : 1.2MB</span><br>
                                <img src="{{ asset($projectById->image) }}" height="80" width="80">
                                <span style="color: red">{{ $errors->has('image') ? $errors->first('image') : ' ' }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Sub Image</label>
                            <div class="col-sm-10">
                                <input type="file" name="sub_image[]" accept="image/*" multiple> <span style="color: red">max size : 5MB</span><br>
                            @foreach($subImageById as $subImageById )
                                    <img src="{{ asset($subImageById->sub_image) }}" height="50" width="50">
                                @endforeach
                                <span style="color: red">{{ $errors->has('sub_image') ? $errors->first('sub_image') : ' ' }}</span>
                            </div>
                            <br>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Description<span style="color: red"> *</span></label>
                            <div class="col-sm-10">
                            <textarea id="description" name="description" class="form-control" rows="50" cols="8" required>
                               <?php echo $projectById->description ?>
                    </textarea>
                                <span style="color: red">{{ $errors->has('description') ? $errors->first('description') : ' ' }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2  control-label">Publication Status<span style="color: red"> *</span></label>
                            <div class="col-sm-10">
                                <select class="form-control" name="publication_status" required>
                                    @if($projectById->publication_status ==1 )
                                    <option value="1">Published</option>
                                    <option value="0">Unpublished</option>
                                        @else
                                        <option value="0">Unpublished</option>
                                        <option value="1">Published</option>
                                        @endif
                                </select>
                                <span style="color: red">{{ $errors->has('publication_status') ? $errors->first('publication_status') : ' ' }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2">
                            </div>
                            <div class="col-sm-10">
                                <button type="submit" name="btn" class="btn btn-info btn-block">Update It</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </form>
        </div>
        <div class="control-sidebar-bg"></div>
    </div>

@endsection

@section('js')
    <script>
        CKEDITOR.replace( 'description',{
            extraPlugins: 'codesnippet'
        });
    </script>
@endsection