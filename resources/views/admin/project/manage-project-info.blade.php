@extends('admin.master')

@section('title')
    <title>Manan Corporation | Project Manage</title>
    @endsection

@section('body')

    <div class="col-md-12" style="margin:50px 0px 0px 00px">
        <h2 class="box-title text-center">Project Table</h2>
        <hr>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Manage Project Info</h3>
                            @if($message = Session::get('message'))
                                <h3 class="text text-success text-center">{{ $message }}</h3>
                            @endif
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>SL No</th>
                                    <th>Project Name</th>
                                    <th>Category Name</th>
                                    <th>Project Image</th>
                                    <th>Project SubImage</th>
                                    <th>Description</th>
                                    <th>Publication Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <?php $sl=1 ?>
                                @foreach($projectInfos as $project)
                                    <tbody>
                                    <tr>
                                        <td>{{ $sl }}</td>
                                        <td>{{ $project->project_name }}</td>
                                        <td>{{ $project->category_name }}</td>
                                        <td><img src="{{ asset($project->image) }}" width="80" height="100"></td>
                                        <td>
                                            <?php $i=0?>
                                            @foreach($subImages as $subImage )
                                                @if($subImage->project_id == $project->id )
                                                        <a href="{{ url('/manan-administration2018/project/delete-sub-image/'.$subImage->id) }}" onclick="return confirm('Are you sure to delete it !!!')" title="delete">
                                                            <img src="{{ asset($subImage->sub_image) }}" width="80" height="100" >
                                                        </a>
                                            <?php $i++ ?>
                                                @endif
                                        @endforeach
                                                @if($i==0)
                                                    {{ 'NULL' }}
                                                @endif
                                        </td>
                                        <td>{{ substr(strip_tags($project->description), 0, 50) }}
                                            <a href="{{ url('manan-administration2018/project/view-project/'.$project->id ) }}">{{ strlen(strip_tags($project->description)) > 30 ? " ...ReadMore" : "" }}</a></td>
                                        <td>
                                            @if($project->publication_status == 1)
                                                {{'Published'}}
                                            @else
                                                {{ 'Unpublished' }}
                                            @endif
                                        </td>

                                        <td>
                                            @if($project->publication_status == 1)
                                                <a href="{{ url('/manan-administration2018/project/unpublished-project/'.$project->id) }}" class="btn btn-success btn-xs" title="published">
                                                    <span class="glyphicon glyphicon-arrow-up"></span>
                                                </a>
                                            @else
                                                <a href="{{ url('/manan-administration2018/project/published-project/'.$project->id) }}" class="btn btn-warning btn-xs" title="unpublished">
                                                    <span class="glyphicon glyphicon-arrow-down"></span>
                                                </a>
                                            @endif

                                            <a href="{{  url('/manan-administration2018/project/editable-project-form/'.$project->id) }}" class="btn btn-primary btn-xs" title="edit">
                                                <span class="glyphicon glyphicon-edit"></span>
                                            </a>
                                            <a href="{{  url('/manan-administration2018/project/delete-project/'.$project->id) }}" onclick="return confirm('Are you sure to delete it !!!')" class="btn btn-primary btn-xs" title="delete">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </a>
                                                <a href="{{  url('/manan-administration2018/project/view-project/'.$project->id) }}" class="btn btn-primary btn-xs" title="delete">
                                                <span class="glyphicon glyphicon-search"></span>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php $sl++ ?>
                                    </tbody>
                                @endforeach
                            </table>
                            <nav class="justify-content-end">
                                {{ $projectInfos->links() }}
                            </nav>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>

        <div class="control-sidebar-bg"></div>
    </div>


@endsection

