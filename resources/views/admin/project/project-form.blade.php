@extends('admin.master')

@section('title')
    <title>Manan Corporation | Project</title>
    @endsection

@section('body')

    <div class="col-md-10" style="margin:50px 0px 0px 100px">
        <h2 class="box-title text-center">Add New Project</h2>
        <hr>
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Project Form</h3>
                @if(Session::has('message'))
                    <h3 class="text text-center text-success">{{ Session::get('message') }}</h3>
                @endif
            </div>

            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="{{ url('/manan-administration2018/project/create-project')}}" method="POST" enctype="multipart/form-data" >
                {{ csrf_field() }}
                <div class="box-body">
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Project Name<span style="color: red"> *</span></label>

                        <div class="col-sm-10">
                            <input type="text" name="project_name" class="form-control"  placeholder="Project Name">
                            <span style="color: red">{{ $errors->has('project_name') ? $errors->first('project_name') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2  control-label">Category Name<span style="color: red"> *</span></label>

                        <div class="col-sm-10">
                            <select class="form-control" name="category_id" required>
                               @foreach($categories as $category )
                                <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                                   @endforeach
                            </select>
                            <span style="color: red">{{ $errors->has('category_id') ? $errors->first('category_id') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Image<span style="color: red"> *</span></label>
                        <div class="col-sm-10">
                            <input type="file" id="image" name="image"/><br><br>
                            <span style="color: red">{{ $errors->has('image') ? $errors->first('image') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Multiple Image</label>
                        <div class="col-sm-10">
                            <input type="file" name="sub_image[]" accept="image/*" multiple><br>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Description<span style="color: red"> *</span></label>
                        <div class="col-sm-10">
                            <textarea id="description" name="description" class="form-control" rows="50" cols="8" required>

                    </textarea>
                            <span style="color: red">{{ $errors->has('description') ? $errors->first('description') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2  control-label">Publication Status<span style="color: red"> *</span></label>

                        <div class="col-sm-10">
                            <select class="form-control" name="publication_status" required>
                                <option value="1">Published</option>
                                <option value="0">Unpublished</option>
                            </select>
                            <span style="color: red">{{ $errors->has('publication_status') ? $errors->first('publication_status') : ' ' }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2">
                        </div>
                        <div class="col-sm-10">
                            <button type="submit" name="btn" class="btn btn-info btn-block">Create Project</button>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->

            </form>
        </div>
        <div class="control-sidebar-bg"></div>
    </div>

    <script>$('#image').bind('change', function() {

            //this.files[0].size gets the size of your file.
            alert(this.files[0].size);

        });</script>

@endsection

@section('js')
    <script>
        CKEDITOR.replace( 'description',{
            extraPlugins: 'codesnippet'
        });
    </script>
@endsection