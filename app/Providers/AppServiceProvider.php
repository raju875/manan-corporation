<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::share('projectCategories',  DB::table('project_categories')->where('publication_status', 1)->get());
        View::share('eventCategories',  DB::table('event_categories')->where('publication_status', 1)->get());
        View::share('blogCategories',  DB::table('blog_categories')->where('publication_status', 1)->get());
        View::share('showLogoMenuBar',  DB::table('logos')->where('publication_status', 1)->orderBy('id','desc')->get());
        View::share('showLogoSinglePage',  DB::table('logos')->where('publication_status', 1)->orderBy('id','desc')->get());
        View::share('countLogo',  DB::table('logos')->where('publication_status', 1)->count());
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
