<?php

namespace App\Http\Controllers;

use App\EventCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;

class EventCategoryController extends Controller
{
    public function eventCategoryForm () {
        //return to event-category form

        return view('admin.event-category.category-event-form');
    }

    public function saveCategoryEvent (Request $request) {
        //save event-category type
        $this->validate($request, [
            'event_type'         => 'required',
            'description'        => 'required',
            'image'              => 'required',
            'publication_status' => 'required',
        ]);

        $image = $request->file('image');
        $imageName = $image->getClientOriginalName();
        $directory = 'admin/event-category-images/';
        $imageUrl = $directory . $imageName;
        Image::make($image)->save($imageUrl);

        $count = DB::table('event_categories')->count();

        if ($count != 0 ) {
            $checks = DB::table('event_categories')->get();

            foreach ($checks as $check ) {
                if ($check->event_type == $request->event_type ) {
                    return redirect('/manan-administration2018/event-category/category-event-form')->with('alert','Category Event already created !!! check it .');
                    }
            }
        }

       $eventCategory = new EventCategory();

       $eventCategory->event_type         = $request->event_type;
       $eventCategory->image              = $imageUrl;
       $eventCategory->description        = $request->description;
       $eventCategory->publication_status = $request->publication_status;

        $eventCategory->save();

        return redirect('/manan-administration2018/event-category/category-event-form')->with('message','save category event-category successfully');
    }

    public function manageCategoryEvent () {
        //manage event-category category return to event-category category table

       $eventCategories = DB::table('event_categories')->orderBy('id','desc')->paginate(5);

       return view('admin.event-category.manage-category-event',[
           'eventCategories' => $eventCategories
       ]);
    }

    public function unpublishedCategoryEvent ($id ) {
        //Unpublished event-category-type info

        DB::table('event_categories')->where('id', $id)->update(['publication_status' => 0]);

        return redirect('/manan-administration2018/event-category/manage-category-event')->with('message', 'Unpublished event-category type info successfully');
    }

    public function publishedCategoryEvent ($id ) {
        //Published event-category-type info

        DB::table('event_categories')->where('id', $id)->update(['publication_status' => 1]);

        return redirect('/manan-administration2018/event-category/manage-category-event')->with('message', 'Published event-category type info successfully');
    }

    public function editableCategoryEventForm ($id ) {
        //return to Editable Event ProjectCategory Form

        $eventCategoryById = EventCategory::find($id);

        return view('admin.event-category.editable-category-event-form',[
            'eventCategoryById' => $eventCategoryById
            ]);
    }

    public function updateCategoryEvent(Request $request ) {
        //update event-category type

        $this->validate($request, [
            'eventCategoryId'    => 'required',
            'event_type'         => 'required',
            'description'        => 'required',
            'publication_status' => 'required',
        ]);

        if ($request->image==null ) {

            $eventCategoryId = EventCategory::find($request->eventCategoryId );

            $eventCategoryId->event_type         = $request->event_type;
            $eventCategoryId->description        = $request->description;
            $eventCategoryId->publication_status = $request->publication_status;

            $eventCategoryId->save();

            return redirect('/manan-administration2018/event-category/manage-category-event')->with('message','Update Event ProjectCategory successfully');
        }

        $image = $request->file('image');
        $imageName = $image->getClientOriginalName();
        $directory = 'admin/event-category-images/';
        $imageUrl = $directory . $imageName;
        Image::make($image)->save($imageUrl);

        $eventCategoryId = EventCategory::find($request->eventCategoryId );

        $eventCategoryId->event_type         = $request->event_type;
        $eventCategoryId->image              = $imageUrl;
        $eventCategoryId->description        = $request->description;
        $eventCategoryId->publication_status = $request->publication_status;

        $eventCategoryId->save();

        return redirect('/manan-administration2018/event-category/manage-category-event')->with('message','Update Event ProjectCategory successfully');
    }

    public function deleteCategoryEvent($id ) {
        //delete event-category category info

        $eventCategoryId = EventCategory::find($id);
        $eventCategoryId->delete();

        return redirect('/manan-administration2018/event-category/manage-category-event')->with('message','Delete Event ProjectCategory successfully');

    }


    public function categoryEventDetails ($id ) {
        $eventCategoryId = EventCategory::find($id);

        return view('admin.event-category.event-category-details',[
            'eventCategoryId' => $eventCategoryId
        ]);
        }
}
