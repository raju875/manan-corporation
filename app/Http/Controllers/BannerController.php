<?php

namespace App\Http\Controllers;

use App\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;

class BannerController extends Controller
{
    public function bannerAddForm () {
        //return to banner form to add new banner
        return view('admin.banner.banner-form');

    }

    public function addNewBanner (Request $request ) {
        // add new banner
//return $request->all();
        $this->validate($request,[
            'banner'             => 'required',
            'publication_status' => 'required'
        ]);

        $bannerFile = $request->file('banner');
        $bannerName = $bannerFile->getClientOriginalName();
        $directory  = 'admin/banner/';
        $url        = $directory.$bannerName;
        Image::make($bannerFile)->save($url);

        $banner = new Banner();
        $banner->banner_title       = $request->banner_title;
        $banner->banner             = $url;
        $banner->description        = $request->description;
        $banner->publication_status = $request->publication_status;
        $banner->save();

        return redirect('/manan-administration2018/banner/banner-add-form')->with('message','Banner added successfully');

    }

    public function bannerManage () {
        //banner manage return to banner table

       $banners = DB::table('banners')->orderBy('id','desc')->paginate(5);
       return view('admin.banner.banner-manage',[
           'banners' => $banners
       ]);
    }

    public function unpublishedBanner ($id) {
        //unpublished banner info
        DB::table('banners')->where('id', $id)->update(['publication_status' => 0]);

        return redirect('/manan-administration2018/banner/banner-manage')->with('message', 'Unpublished banner info successfully');
    }

    public function publishedBanner ($id) {
        //published banner info
        DB::table('banners')->where('id', $id)->update(['publication_status' => 1]);

        return redirect('/manan-administration2018/banner/banner-manage')->with('message', 'Published banner info successfully');
    }

    public function editableBannerForm ($id ) {
        //return to editable banner form to update banner

       $bannerById = Banner::find($id);
       return view('admin.banner.editable-banner-form',[
           'bannerById' => $bannerById
       ]);
    }

    public function updateBannerInfo (Request $request ) {
        //update banner info
        $this->validate($request,[
            'publication_status' => 'required',
        ]);

        if ($request->file('banner') == null) {
            $banner = Banner::find($request->id);
            $banner->banner_title       = $request->banner_title;
            $banner->description        = $request->description;
            $banner->publication_status = $request->publication_status;
            $banner->save();

            return redirect('/manan-administration2018/banner/banner-manage')->with('message','Banner update successfully');
        }else {
            $bannerFile = $request->file('banner');
            $bannerName = $bannerFile->getClientOriginalName();
            $directory  = 'admin/banner/';
            $url        = $directory.$bannerName;
            Image::make($bannerFile)->save($url);

            $banner = Banner::find($request->id);
            $banner->banner_title       = $request->banner_title;
            $banner->banner             = $url;
            $banner->description        = $request->description;
            $banner->publication_status = $request->publication_status;
            $banner->save();

            return redirect('/manan-administration2018/banner/banner-manage')->with('message','Banner update successfully');
        }

    }



     public function deleteBannerInfo ($id) {
        //delete banner info
         Banner::find($id)->delete();
         return redirect('/manan-administration2018/banner/banner-manage')->with('message','Banner delete successfully');
     }

     public function BannerView ($id ) {
        //banner view
        $banner = Banner::find($id);
        return view('admin.banner.view-banner',[
            'banner' => $banner
        ]);
     }

}
