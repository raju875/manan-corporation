<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Project;
use App\SubImage;
use App\ProjectCategory;
use Image;

class ProjectController extends Controller
{
    public function projectForm()
    {
        //return to project form to add new project

        $rows = DB::table('project_categories')->count();
        if ($rows != 0) {
            $count = DB::table('project_categories')->where('publication_status', 1)->count();

            if ($count != 0) {
                $categories = DB::table('project_categories')->where('publication_status', 1)->get();

                return view('admin.project.project-form', [
                    'categories' => $categories
                ]);
            } else {
                $count1 = DB::table('project_categories')->where('publication_status', 0)->count();

                $aleart = 'All categories are unpublished !!!Update it and try again';
                return redirect('/manan-administration2018/error/error-500/' . $aleart);
            }
        } else {
            $aleart = 'ProjectCategory is empty!!!Create some ProjectCategory and try again';
            return redirect('/manan-administration2018/error/error-500/' . $aleart);
        }
    }

    public function createProject(Request $request)
    {
        //Create a new project

        $this->validate($request, [
            'project_name'       => 'required',
            'category_id'        => 'required',
            'description'        => 'required',
            'image'              => 'required',
            'publication_status' => 'required',
        ]);

        $image = $request->file('image');
        $imageName = $image->getClientOriginalName();
        $directory = 'admin/project-images/';
        $imageUrl = $directory . $imageName;
        Image::make($image)->save($imageUrl);

        $project = new Project();

        $project->project_name = $request->project_name;
        $project->category_id = $request->category_id;
        $project->image = $imageUrl;
        $project->description = $request->description;
        $project->publication_status = $request->publication_status;

        $project->save();
        $projectId = $project->id;

        if ($request->file('sub_image')!=null ) {
            $projectSubImages = $request->file('sub_image');

            foreach ($projectSubImages as $projectSubImage) {
                $SubImageName = $projectSubImage->getClientOriginalName();
                $subImageDirectory = 'admin/project-sub-images/';
                $subImageUrl = $subImageDirectory . $SubImageName;
                Image::make($projectSubImage)->save($subImageUrl);

                $subImage = new SubImage();
                $subImage->project_id = $projectId;
                $subImage->sub_image = $subImageUrl;
                $subImage->save();
            }
        }

        return redirect('/manan-administration2018/project/project-form')->with('message', 'Project create successfully');
    }

    public function manageProject()
    {
        //Manage project info return to project manage table

        $projectInfos = DB::table('projects')->join('project_categories', 'projects.category_id', '=', 'project_categories.id')->select('projects.*', 'project_categories.category_name')->orderBy('id', 'desc')->paginate(5);
        $subImages    = DB::table('sub_images')->orderBy('id', 'desc')->get();

        return view('admin.project.manage-project-info', [
            'projectInfos' => $projectInfos,
            'subImages'    => $subImages
        ]);
    }

    public function unpublishedProject($id)
    {
        //Unpublished project info

        DB::table('projects')->where('id', $id)->update(['publication_status' => 0]);

        return redirect('/manan-administration2018/project/manage-project')->with('message', 'Unpublished project info successfully');

    }

    public function publishedProject($id)
    {
        //Published project info

        DB::table('projects')->where('id', $id)->update(['publication_status' => 1]);

        return redirect('/manan-administration2018/project/manage-project')->with('message', 'Published project info successfully');
    }

    public function editableProjectForm($id)
    {
        //editable project form

        $projectById = Project::find($id);
        $categoryById = ProjectCategory::find($projectById->category_id);
        $subImageById = SubImage:: where('sub_images.project_id', $id)->get();

        return view('admin.project.editable-project-form', [
            'projectById' => $projectById,
            'categoryById' => $categoryById,
            'subImageById' => $subImageById

        ]);
    }

    public function updateProject(Request $request ){
        //Update project info
        $this->validate($request, [
            'project_name'       => 'required',
            'description'        => 'required',
            'publication_status' => 'required',
        ]);

        $projectInfos = Project::all();

            if ($request->image==null && $request->sub_image==null ) {
                    $projectById = Project::find($request->project_id);

                    $projectById->project_name = $request->project_name;
                    $projectById->description = $request->description;
                    $projectById->publication_status = $request->publication_status;

                    $projectById->save();
                    return redirect('/manan-administration2018/project/manage-project')->with('message', 'Update project info succussfully');
                }

            if ($request->image!=null && $request->sub_image==null ){
                $productImage = $request->file('image');
                    $imageName    = $productImage->getClientOriginalName();
                    $directory    = 'admin/project-images/';
                    $url          = $directory . $imageName;
                    Image::make($productImage)->save($url);

                    $projectById = Project::find($request->project_id );

                    $projectById->project_name       = $request->project_name;
                    $projectById->image              = $url;
                    $projectById->description        = $request->description;
                    $projectById->publication_status = $request->publication_status;

                    $projectById->save();
                    return redirect('/manan-administration2018/project/manage-project')->with('message','Update project info succussfully');

                }

            if ($request->image==null && $request->sub_image!=null ) {
                $count = DB::table('sub_images')->where('project_id',$request->project_id)->count();
                if ($count==0 ) {
                    $projectById = Project::find($request->project_id);

                    $projectById->project_name       = $request->project_name;
                    $projectById->description        = $request->description;
                    $projectById->publication_status = $request->publication_status;

                    $projectById->save();

                    $projectSubImages = $request->file('sub_image');

                    foreach ($projectSubImages as $projectSubImage) {
                        $SubImageName      = $projectSubImage->getClientOriginalName();
                        $subImageDirectory = 'admin/project-sub-images/';
                        $subImageUrl       = $subImageDirectory . $SubImageName;
                        Image::make($projectSubImage)->save($subImageUrl);

                        $subImage             = new SubImage();
                        $subImage->project_id = $request->project_id;
                        $subImage->sub_image  = $subImageUrl;

                        $subImage->save();
                    }
                    return redirect('/manan-administration2018/project/manage-project')->with('message', 'Update project info succussfully');

                }else {
                   //return SubImage::find(30);
                    DB::table('sub_images')->where('project_id',$request->project_id)->delete();
                    $projectById = Project::find($request->project_id);

                    $projectById->project_name       = $request->project_name;
                    $projectById->description        = $request->description;
                    $projectById->publication_status = $request->publication_status;

                    $projectById->save();

                    $projectSubImages = $request->file('sub_image');
                    foreach ($projectSubImages as $projectSubImage) {
                        $SubImageName      = $projectSubImage->getClientOriginalName();
                        $subImageDirectory = 'admin/project-sub-images/';
                        $subImageUrl       = $subImageDirectory . $SubImageName;
                        Image::make($projectSubImage)->save($subImageUrl);

                        $subImage = new SubImage();

                        $subImage->project_id = $request->project_id;
                        $subImage->sub_image  = $subImageUrl;
                        $subImage->save();
                    }
                    return redirect('/manan-administration2018/project/manage-project')->with('message', 'Update project info succussfully');
                }

                return redirect('/manan-administration2018/project/manage-project')->with('message','Update project info succussfully');
            }

            if ($request->image!=null && $request->sub_image!=null ) {
                $productImage = $request->file('image');
                $imageName    = $productImage->getClientOriginalName();
                $directory    = 'admin/project-images/';
                $url          = $directory . $imageName;
                Image::make($productImage)->save($url);

                $projectById = Project::find($request->project_id );

                $projectById->project_name       = $request->project_name;
                $projectById->image              = $url;
                $projectById->description        = $request->description;
                $projectById->publication_status = $request->publication_status;

                $projectById->save();
                $projectById = Project::find($request->project_id);

                $projectById->project_name       = $request->project_name;
                $projectById->description        = $request->description;
                $projectById->publication_status = $request->publication_status;

                $projectById->save();

                $projectSubImages = $request->file('sub_image');

                foreach ($projectSubImages as $projectSubImage) {
                    $SubImageName      = $projectSubImage->getClientOriginalName();
                    $subImageDirectory = 'admin/project-sub-images/';
                    $subImageUrl       = $subImageDirectory . $SubImageName;
                    Image::make($projectSubImage)->save($subImageUrl);

                    $subImage             = new SubImage();
                    $subImage->project_id = $request->project_id;
                    $subImage->sub_image  = $subImageUrl;

                    $subImage->save();
                }

            }
       return redirect('/manan-administration2018/project/manage-project')->with('message','Update project info succussfully');
    }


    public function deleteProject($id ) {
        //delete project info

        $projectById = Project::find($id);
        $projectById->delete();
        $subImageById = SubImage:: where('sub_images.project_id',$id)->get();
        foreach($subImageById as $subImageById) {
            $subImageById->delete();
        }
        return redirect('/manan-administration2018/project/manage-project')->with('message','Project info delete successfully');

    }

    public function viewProject ($id ) {
        //view project info by id

        $projectById = Project::find($id);
        $projectCategoryById = ProjectCategory::find($projectById->category_id);
        $subImageById        = DB::table('sub_images')->where('project_id',$projectById->id)->get();
        return view('admin.project.project-view',[
            'projectById'         => $projectById,
            'projectCategoryById' => $projectCategoryById,
            'subImageById'        => $subImageById,
        ]);
    }


    public function deleteSubImages($id) {
        SubImage::find($id)->delete();
        return redirect('/manan-administration2018/project/manage-project')->with('message','Sub Image delete successfully');
    }
}
