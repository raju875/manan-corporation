<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Gallery;
use Image;
class GalleryController extends Controller
{

    public function galleryAddForm() {
        return view('admin.gallery.gallery-image-add-form');
    }

    public function imageAddToGallery(Request $request) {
        $this->validate($request, [
            'gallery_image'      => 'required',
            'publication_status' => 'required',
        ]);

        $galleryImages = $request->file('gallery_image');

        foreach ($galleryImages as $galleryImage) {
            $imageName = $galleryImage->getClientOriginalName();
            $imageDirectory = 'admin/gallery-images/';
            $imageUrl = $imageDirectory . $imageName;
            Image::make($galleryImage)->save($imageUrl);

            $gallery = new Gallery();
            $gallery->gallery_image       = $imageUrl;
            $gallery->publication_status  = $request->publication_status;

            $gallery->save();
        }

        return redirect('/manan-administration2018/gallery/gallery-add-form')->with('message', 'Gallery image added successfully');
    }

    public function galleryManage() {

        $images = DB::table('galleries')->orderBy('id','desc')->paginate(5);

        return view('admin.gallery.gallery-image-manage',[
            'images' => $images
        ]);
    }

    public function unpublishedGalleryImage($id ) {
        DB::table('galleries')->where('id', $id)->update(['publication_status' => 0]);

        return redirect('/manan-administration2018/gallery/manage-gallery')->with('message', 'Unpublished Gallery Images successfully');
    }

    public function publishedGalleryImage($id ) {
        DB::table('galleries')->where('id', $id)->update(['publication_status' => 1]);

        return redirect('/manan-administration2018/gallery/manage-gallery')->with('message', 'Published Gallery Images successfully');
    }

    public function deleteGalleryImage($id ) {
        DB::table('galleries')->where('id', $id)->delete();

        return redirect('/manan-administration2018/gallery/manage-gallery')->with('message', 'Delete Gallery Images successfully');
    }

    public function allGalleryImage() {
        $projects  = DB::table('projects')->where('publication_status',1)->get();
        $events    = DB::table('events')->where('publication_status',1)->get();
        $blogs     = DB::table('blogs')->where('publication_status',1)->get();
        $galleries = DB::table('galleries')->where('publication_status',1)->get();

        return view('front.gallery.gallery',[
            'projects'  => $projects,
            'events'    => $events,
            'blogs'     => $blogs,
            'galleries' => $galleries
        ]);
    }

    public function projectGalleryPanel() {
        $projects = DB::table('projects')->where('publication_status',1)->get();
        return view('front.gallery.project-gallery-page',[
            'projects' => $projects
        ]);
    }

    public function eventGalleryPanel() {
        $events = DB::table('events')->where('publication_status',1)->get();
        return view('front.gallery.event-gallery-page',[
            'events' => $events
        ]);
    }

    public function blogGalleryPanel() {
        $blogs = DB::table('blogs')->where('publication_status',1)->get();
        return view('front.gallery.blog-gallery-page',[
            'blogs' => $blogs
        ]);
    }

    public function otherGalleryPanel() {
        $galleries = DB::table('galleries')->where('publication_status',1)->get();
        return view('front.gallery.other-gallery-page',[
            'galleries' => $galleries
        ]);
    }
}
