<?php

namespace App\Http\Controllers;

use App\Logo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;

class LogoController extends Controller
{
    public function logoAddForm() {
        return view('admin.logo.logo-form');
    }

    public function addNewLogo(Request $request ) {
        //return $request->all();
        $this->validate($request,[
            'logo'               => 'required',
            'publication_status' => 'required'
        ]);

        $logoFile   = $request->file('logo');
        $logoName   = $logoFile->getClientOriginalName();
        $directory  = 'admin/logos/';
        $url        = $directory.$logoName;
        Image::make($logoFile)->save($url);

        $logo = new Logo();

        $logo->logo_title         = $request->logo_title;
        $logo->logo               = $url;
        $logo->description        = $request->description;
        $logo->publication_status = $request->publication_status;
        $logo->save();

        return redirect('/manan-administration2018/logo/logo-add-form')->with('message','Logo added successfully');
    }

    public function manageLogo() {
        $logos = DB::table('logos')->orderBy('id','desc')->paginate(5);
        return view('admin.logo.manage-logo',[
            'logos' => $logos
        ]);

    }

    public function unpublishedLogo($id ) {
        DB::table('logos')->where('id', $id)->update(['publication_status' => 0]);
        return redirect('/manan-administration2018/logo/logo-manage')->with('message', 'Unpublished logo info successfully');
    }


    public function publishedLogo($id ) {
        DB::table('logos')->where('id', $id)->update(['publication_status' => 1]);
        return redirect('/manan-administration2018/logo/logo-manage')->with('message', 'Published logo info successfully');
    }


    public function editableLogoForm($id ) {
       $logoById = Logo::find($id);
       return view('admin.logo.editable-logo-form',[
           'logoById' => $logoById
       ]);
    }


    public function updateLogo(Request $request ) {
        if ($request->file('logos') == null ) {
            $logo = Logo::find($request->id);

            $logo->logo_title         = $request->logo_title;
            $logo->description        = $request->description;
            $logo->publication_status = $request->publication_status;
            $logo->save();

            return redirect('/manan-administration2018/logo/logo-manage')->with('message','Logo Info update successfully');
        }else {
            $logoFile   = $request->file('logo');
            $logoName   = $logoFile->getClientOriginalName();
            $directory  = 'admin/logos/';
            $url        = $directory.$logoName;
            Image::make($logoFile)->save($url);

            $logo = new Logo();

            $logo->logo_title         = $request->logo_title;
            $logo->logo               = $url;
            $logo->description        = $request->description;
            $logo->publication_status = $request->publication_status;
            $logo->save();

            return redirect('/manan-administration2018/logo/logo-manage')->with('message','Logo Info update successfully');
        }

    }


    public function deleteLogo($id ) {
        Logo::find($id)->delete();
        return redirect('/manan-administration2018/logo/logo-manage')->with('message','Logo Info delete successfully');

    }


    public function viewLogo($id ) {
        $logoById = Logo::find($id);
        return view('admin.logo.view-logo',[
            'logoById' => $logoById
        ]);

    }
}
