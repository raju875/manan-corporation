<?php

namespace App\Http\Controllers;

use App\ProjectCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;

class ProjectCategoryController extends Controller
{
    public function categoryProjectForm() {
        //ProjectCategory form

        return view('admin.project-category.category-project-form');
        }


    public function saveCategoryProjectInfo(Request $request ) {
        //save ProjectCategory inforlmation

        $this->validate($request,[
            'category_name'      => 'required',
            'category_image'     => 'required',
            'publication_status' => 'required'
        ]);
//return $request->all();

        $count = DB::table('project_categories')->count();

        if ($count !=0 ) {
            $checks = DB::table('project_categories')->get();

            foreach ($checks as $check ) {
                if ($check->category_name == $request->category_name ) {
                    return redirect('/manan-administration2018/project-category/category-project-form')->with('alert','Project Category already created !!! check it .');
                }
            }
        }

        $image = $request->file('category_image');
        $imageName = $image->getClientOriginalName();
        $directory = 'admin/project-category-images/';
        $imageUrl = $directory . $imageName;
        Image::make($image)->save($imageUrl);

        $category = new ProjectCategory();
        $category->category_name       = $request->category_name;
        $category->category_image      = $imageUrl;
        $category->description         = $request->description;
        $category->publication_status  = $request->publication_status;

        $category->save();

        return redirect('/manan-administration2018/project-category/category-project-form')->with('message','ProjectCategory info saved successfully');
        }


     public function manageCategoryProjectInfo() {
        //ProjectCategory Manage table

       $categories = DB::table('project_categories')->orderBy('id','desc')->paginate(5);
        return view('admin.project-category.manage-category-project-info',[
            'categories' => $categories
        ]);
        }


    public function unpublishedCategoryProjectInfo($id ){
        //Unpublished project-category info from project-category manage table

        DB::table('project_categories')->where('id', $id)->update(['publication_status' => 0]);

        return redirect('/manan-administration2018/project-category/manage-category-project')->with('message','Unpublished project-category info successfully');
        }


    public function publishedCategoryProjectInfo($id ){
        //Published project-category info from project-category manage table

        DB::table('project_categories')->where('id', $id)->update(['publication_status' => 1]);

        return redirect('/manan-administration2018/project-category/manage-category-project')->with('message','Published project-category info successfully');
        }


    public function editableCategoryProjectForm($id) {
        // return to editable project-category form

        $categoryInfos = DB::table('project_categories')->where('id', $id)->get();

        return view('admin.project-category.editable-category-project-form',[
            'categoryInfos' => $categoryInfos
        ]);
        }


     public function updateCategoryProjectInfo(Request $request) {
        //Update ProjectCategory info

        $this->validate($request,[
            'category_name'      => 'required',
            'publication_status' => 'required'
        ]);
        //return $request->all();

         if ($request->category_image == null ) {

             $categoryById = ProjectCategory::find($request->id);
             $categoryById->category_name       = $request->category_name;
             $categoryById->description         = $request->description;
             $categoryById->publication_status  = $request->publication_status;

             $categoryById -> save();

             return redirect('/manan-administration2018/project-category/manage-category-project')->with('message','Update project-category info successfully');

         }else {
             $image = $request->file('category_image');
             $imageName = $image->getClientOriginalName();
             $directory = 'admin/project-category-images/';
             $imageUrl = $directory . $imageName;
             Image::make($image)->save($imageUrl);

             $categoryById = ProjectCategory::find($request->id);

             $categoryById->category_name       = $request->category_name;
             $categoryById->category_image       = $imageUrl;
             $categoryById->description         = $request->description;
             $categoryById->publication_status  = $request->publication_status;

             $categoryById -> save();

             return redirect('/manan-administration2018/project-category/manage-category-project')->with('message','Update project-category info successfully');
         }

        }


    public function deleteCategoryProjectInfo($id) {
        //delete about row or info

        DB::table('project_categories')->where('id', $id)-> delete();

        return redirect('/manan-administration2018/project-category/manage-category-project')->with('message','Delete project-category info successfully');
        }


    public function projectCategoryDetails($id ) {
        //delete about row or info
        $projectCategoryById = ProjectCategory::find($id);

        return view('admin.project-category.project-category-details',[
            'projectCategoryById' => $projectCategoryById
        ]);
        }

}
