<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContactController extends Controller
{
    public function submitContactForm(Request $request ) {

        $this->validate($request,[
            'name'   => 'required',
            'email'  => 'required',
            'topics' => 'required'
        ]);

        $contact = new Contact();
        $contact->name   = $request->name;
        $contact->email  = $request->email;
        $contact->date  = $request->date;
        $contact->time  = $request->time;
        $contact->topics = $request->topics;
        $contact->save();
        return redirect('/')->with('message','Submitted contact form successfully');

    }

    public function manageContact() {
       $contactLists = DB::table('contacts')->orderBy('id','desc')->paginate(5);
       return view('admin.contact.contact-list',[
           'contactLists' => $contactLists
       ]);
    }

    public function contactDetailsPage($id ) {
       $contact = Contact::find($id );
       return view('admin.contact.contact-details',[
           'contact' => $contact
       ]);
    }

    public function contactDelete($id ) {
        Contact::find($id)->delete();
        return redirect('/manan-administration2018/contact/contact-list')->with('message','Contact info delete successfully');
    }
}
