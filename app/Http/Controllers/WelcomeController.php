<?php

namespace App\Http\Controllers;

use App\Blog;
use App\BlogCategory;
use App\Event;
use App\ProjectCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Project;
use App\EventCategory;

class WelcomeController extends Controller
{
    public function index () {
        //return to front madter

        $abouts            = DB::table('abouts')->where('publication_status',1)->get();
        $projects          = DB::table('projects')->where('publication_status',1)->take(12)->get();
        $events            = DB::table('events')->where('publication_status',1)->take(6)->orderBy('id','desc')->get();
        $blogs             = DB::table('blogs')->join('blog_categories','blogs.blog_category_id','=','blog_categories.id')->where('blogs.publication_status',1)->select('blogs.*','blog_categories.blog_category')->take(12)->get();

        return view('front.index.index',[
            'abouts'       => $abouts,
            'projects'     => $projects,
            'events'       => $events,
            'blogs'        => $blogs
        ]);
    }


    public function aboutUs () {
       $abouts = DB::table('abouts')->where('publication_status',1)->get();
        return view('front.about.about-us',[
            'abouts' => $abouts
        ]);
    }


    public function blog () {
        return view('front.blog.blog');
    }


    public function allProjectCategories () {

        $projectCategories  = DB::table('project_categories')->where('publication_status',1)->get();
        return view('front.project.all-project-categories',[
            'projectCategories'  => $projectCategories
        ]);

    }

    public function selectProjectCategory ($id) {
        //return $id;
        $selectCategoryName = ProjectCategory::find($id);
        $projects           = DB::table('projects')->where('category_id',$id)->where('publication_status',1)->get();
        $projectCategories  = DB::table('project_categories')->where('publication_status',1)->get();
        return view('front.project.project-category-select',[
            'selectCategoryName' => $selectCategoryName,
            'projects'           => $projects
        ]);

    }


    public function selectProjectById ($id) {
        //return $id;
        $selectProject     = Project::find($id);
        $projectCategories = DB::table('project_categories')->where('publication_status',1)->get();
        $eventCategories   = DB::table('event_categories')->where('publication_status',1)->get();
        $blogCategories    = DB::table('blog_categories')->where('publication_status',1)->get();
        $projectRowCount   = DB::table('projects')->select('category_id', DB::raw('count(*) as total'))->where('publication_status',1)->groupBy('category_id')->get();
        $eventRowCount     = DB::table('events')->select('event_category_id', DB::raw('count(*) as total'))->where('publication_status',1)->groupBy('event_category_id')->get();
        $blogRowCount      = DB::table('blogs')->select('blog_category_id', DB::raw('count(*) as total'))->where('publication_status',1)->groupBy('blog_category_id')->get();
        return view('front.project.project-select-single-page',[
            'selectProject'     => $selectProject,
            'projectCategories' => $projectCategories,
            'eventCategories'   => $eventCategories,
            'blogCategories'    => $blogCategories,
            'projectRowCount'   => $projectRowCount,
            'eventRowCount'     => $eventRowCount,
            'blogRowCount'      => $blogRowCount
        ]);
    }


    public function allEventCategories () {

        $eventCategories  = DB::table('event_categories')->where('publication_status',1)->get();
        return view('front.event.all-event-categories',[
            'eventCategories'  => $eventCategories
        ]);
    }


    public function selectEventCategory ($id ) {
        //return $id;
        $eventCategoryById  = EventCategory::find($id);
        $events             = DB::table('events')->where('event_category_id',$id)->where('publication_status',1)->get();
        return view('front.event.event-category-select',[
            'eventCategoryById' => $eventCategoryById,
            'events'            => $events
        ]);
    }


    public function eventSelect ($id ) {
        //return $id;
        $eventById = Event::find($id);
        $eventCategoryById = DB::table('events')->join('event_categories','events.event_category_id','=','event_categories.id')->where('events.id',$id)->where('events.publication_status',1)->select('event_categories.event_type')->get();
        $projectCategories = DB::table('project_categories')->where('publication_status',1)->get();
        $eventCategories   = DB::table('event_categories')->where('publication_status',1)->get();
        $blogCategories    = DB::table('blog_categories')->where('publication_status',1)->get();
        $projectRowCount   = DB::table('projects')->select('category_id', DB::raw('count(*) as total'))->where('publication_status',1)->groupBy('category_id')->get();
        $eventRowCount     = DB::table('events')->select('event_category_id', DB::raw('count(*) as total'))->where('publication_status',1)->groupBy('event_category_id')->get();
        $blogRowCount      = DB::table('blogs')->select('blog_category_id', DB::raw('count(*) as total'))->where('publication_status',1)->groupBy('blog_category_id')->get();
        return view('front.event.event-select-single-page',[
            'eventById'         => $eventById,
            'eventCategoryById' => $eventCategoryById,
            'projectCategories' => $projectCategories,
            'eventCategories'   => $eventCategories,
            'blogCategories'    => $blogCategories,
            'projectRowCount'   => $projectRowCount,
            'eventRowCount'     => $eventRowCount,
            'blogRowCount'      => $blogRowCount
        ]);
    }


    public function allBlogCategories () {
        $blogCategories  = DB::table('blog_categories')->where('publication_status',1)->get();
        return view('front.blog.all-blog-categories',[
            'blogCategories'  => $blogCategories
        ]);
    }


    public function selectBlogCategory ($id ) {
        $blogCategoryById   = BlogCategory::find($id);
        $blogs              = DB::table('blogs')->where('blog_category_id',$id)->where('publication_status',1)->get();
        return view('front.blog.blog-category-select',[
            'blogCategoryById' => $blogCategoryById,
            'blogs'            => $blogs
        ]);
    }


    public function selectBlogById ($id ) {
        $selectBlog        = Blog::find($id);
        $projectCategories = DB::table('project_categories')->where('publication_status',1)->get();
        $eventCategories   = DB::table('event_categories')->where('publication_status',1)->get();
        $blogCategories    = DB::table('blog_categories')->where('publication_status',1)->get();
        $projectRowCount   = DB::table('projects')->select('category_id', DB::raw('count(*) as total'))->where('publication_status',1)->groupBy('category_id')->get();
        $eventRowCount     = DB::table('events')->select('event_category_id', DB::raw('count(*) as total'))->where('publication_status',1)->groupBy('event_category_id')->get();
        $blogRowCount      = DB::table('blogs')->select('blog_category_id', DB::raw('count(*) as total'))->where('publication_status',1)->groupBy('blog_category_id')->get();
        return view('front.blog.blog-select-single-page',[
            'selectBlog'        => $selectBlog,
            'projectCategories' => $projectCategories,
            'eventCategories'   => $eventCategories,
            'blogCategories'    => $blogCategories,
            'projectRowCount'   => $projectRowCount,
            'eventRowCount'     => $eventRowCount,
            'blogRowCount'      => $blogRowCount
        ]);
    }

}
