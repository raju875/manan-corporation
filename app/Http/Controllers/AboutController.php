<?php

namespace App\Http\Controllers;

use App\About;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AboutController extends Controller
{
    public function aboutForm() {
        return view ('admin.about.about-form');
    }

    public function addNewAbout(Request $request) {
        //save aboutUs in database

        //return $request->all();
        $this->validate($request,[
            'title'              => 'required',
            'description'        => 'required',
            'publication_status' => 'required'
        ]);

        $about = new About();
        $about->title              = $request->title;
        $about->description        = $request->description;
        $about->publication_status = $request->publication_status;

        $about->save();

        return redirect('/manan-administration2018/about/about-form')->with('message','AboutUs save successfully');

    }

    public function manageAbout() {
        //return to about table and manage it

        $abouts = DB::table('abouts')->orderBy('id','desc')->paginate(5);
        return view('admin.about.manage-about',[
            'abouts' => $abouts
        ]);
    }

    public function unpublishedAbout($id) {
        //unpublished publiched about from about table

        DB::table('abouts')->where('id', $id)->update(['publication_status' => 0]);

        return redirect('/manan-administration2018/about/manage-about')->with('message','unpublished info successfully');

    }

    public function publishedAbout($id) {
        //published unpubliched about from about table

        DB::table('abouts')->where('id', $id)->update(['publication_status' => 1]);

        return redirect('/manan-administration2018/about/manage-about')->with('message','published info successfully');

    }

    public function editableAboutForm($id) {
        //return to editable about form

       $aboutInfos = DB::table('abouts')->where('id', $id)->get();

        return view('admin.about.editable-about-form',[
            'aboutInfos' => $aboutInfos
        ]);

    }

    public function updateAbout(Request $request) {
        //Update about info

        $this->validate($request,[
            'title'              => 'required',
            'description'        => 'required',
            'publication_status' => 'required'
        ]);

        $aboutById = About::find($request->id);

        $aboutById->title               = $request->title;
        $aboutById->description         = $request->description;
        $aboutById->publication_status  = $request->publication_status;

        $aboutById -> save();

        return redirect('/manan-administration2018/about/manage-about')->with('message','Update about info successfully');
    }

    public function deleteAbout($id) {
        //delete about row or info

        DB::table('abouts')->where('id', $id)-> delete();

        return redirect('/manan-administration2018/about/manage-about')->with('message','Delete about info successfully');

    }

    public function viewAbout($id) {
        //view about info

        $bannerById = About::find($id );

        return view('admin.about.view-about-info',[
            'aboutById' => $bannerById
        ]);

    }

}
