<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;

class ProfileController extends Controller
{
    public function profile(){
        return view('admin.profile.profile',array('user' => Auth::user()) );
    }

    public function updateProfile(Request $request){
        //Handle and update admin profile image

        //return $request->all();
        if ($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $fileName = time().'.'.$avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 200)->save(public_path('admin/loginProfileImages/'.$fileName ));

            $user = Auth::user();
            $user->avatar = $fileName;
            $user->save();

            return view('admin.profile.profile',array('user' => Auth::user()) );

        }

    }
}
