<?php

namespace App\Http\Controllers;

use App\BlogCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;

class BlogCategoryController extends Controller
{
    public function blogCategoryForm () {
        //add new blog-category category return to blog-category category form

        return view('admin.blog-category.blog-category-form');
    }

    public function addNewBlogCategory (Request $request ) {
        //add new blog-category category

        $this->validate($request, [
            'blog_category'      => 'required',
            'image'              => 'required',
            'publication_status' => 'required',
        ]);

        $count = DB::table('blog_categories')->count();
        if ($count !=0 ) {
            $blogs = BlogCategory::all();
            foreach ($blogs as $blog ) {
                if ($blog->blog_category == $request->blog_category ) {

                    return redirect('/manan-administration2018/blog-category/blog-category-form')->with('aleart','Blog Category already created!!!');
                }
            }
        }

        $image     = $request->file('image');
        $imageName = $image->getClientOriginalName();
        $directory = 'admin/blog-category-images/';
        $imageUrl  = $directory . $imageName;
        Image::make($image)->save($imageUrl);

        $blogCategory = new  BlogCategory();

        $blogCategory->blog_category      = $request->blog_category;
        $blogCategory->image              = $imageUrl;
        $blogCategory->description        = $request->description;
        $blogCategory->publication_status = $request->publication_status;

        $blogCategory->save();

        return redirect('/manan-administration2018/blog-category/blog-category-form')->with('message','Blog Category added successfully');
    }

    public function manageBlogCategory () {
        //manage blog-category category return to manage blog-category category table
        $blogCategories = DB::table('blog_categories')->orderBy('id','desc')->paginate(5);

       return view('admin.blog-category.blog-category-manage',[
           'blogCategories' => $blogCategories
       ]);
    }

    public function unpublishedBlogCategory ($id ) {
        //unpublished blog category

        DB::table('blog_categories')->where('id', $id)->update(['publication_status' => 0 ]);

        return redirect('/manan-administration2018/blog-category/manage-blog-category')->with('message', 'Unpublished blog category info successfully');
    }

    public function publishedBlogCategory ($id ) {
        //published blog category

        DB::table('blog_categories')->where('id', $id)->update(['publication_status' => 1 ]);

        return redirect('/manan-administration2018/blog-category/manage-blog-category')->with('message', 'Published blog category info successfully');
    }

    public function editableBlogCategoryForm ($id ) {
        //edit blog category return to editable blog category form

        $blogCategoryById = BlogCategory::find($id );

        return view('admin.blog-category.editable-blog-category-form', [
            'blogCategoryById' => $blogCategoryById
        ]);
    }

    public function updateBlogCategoryInfo (Request $request ) {
        //update blog category info

        $this->validate($request, [
            'blog_category'      => 'required',
            'description'        => 'required',
            'publication_status' => 'required',
        ]);
        //return $request->all();
        if ($request->file('image') == null) {

            $blogCategoryById = BlogCategory::find($request->blog_category_id );

            $blogCategoryById->blog_category      = $request->blog_category;
            $blogCategoryById->image              = $blogCategoryById->image;
            $blogCategoryById->description        = $request->description;
            $blogCategoryById->publication_status = $request->publication_status;

            $blogCategoryById->save();
            return redirect('/manan-administration2018/blog-category/manage-blog-category')->with('message','Update Blog Category successfully');

        } else {
            $blogCategoryById = BlogCategory::find($request->blog_category_id );

            $blogCategoryById->blog_category      = $request->blog_category;
            $blogCategoryById->description        = $request->description;
            $blogCategoryById->publication_status = $request->publication_status;

            $blogCategoryById->save();

            return redirect('/manan-administration2018/blog-category/manage-blog-category')->with('message','Update Blog Category successfully');
        }
    }


    public function deleteBlogCategoryInfo($id ) {
        //delete blog category info

        $blogCategoryById = BlogCategory::find($id);
        $blogCategoryById->delete();

        return redirect('/manan-administration2018/blog-category/manage-blog-category')->with('message','Blog Category info delete successfully');
        }


    public function viewBlogCategoryInfo ($id ) {

        $blogCategories = BlogCategory::find($id);
        return view('admin.blog-category.view-blog-category',[
            'blogCategories' => $blogCategories
        ]);
        }

}
