<?php

namespace App\Http\Controllers;

use App\Event;
use App\EventCategory;
use Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EventController extends Controller
{
    public function eventForm () {
        //return to create event form

        $rows = DB::table('event_categories')->count();
        if ($rows != 0) {
            $count = DB::table('event_categories')->where('publication_status', 1)->count();

            if ($count != 0) {
                $categories = DB::table('event_categories')->where('publication_status', 1)->get();

                return view('admin.event.event-form', [
                    'categories' => $categories
                ]);
            } else {
                $count1 = DB::table('project_categories')->where('publication_status', 0)->count();

                $aleart = 'All categories are unpublished !!!Update it and try again';
                return redirect('/manan-administration2018/error/error-500/' . $aleart);
            }
        } else {
            $aleart = 'Event Category is empty!!!Create some ProjectCategory and try again';
            return redirect('/manan-administration2018/error/error-500/' . $aleart);
        }

    }

    public function createNewEvent (Request $request ) {
        //save and create new event
      //  return $request->all();

        $this->validate($request, [
            'event_name'         => 'required',
            'event_category_id'  => 'required',
            'location'           => 'required',
            'start_date'         => 'required',
            'start_time'         => 'required',
            'end_date'           => 'required',
            'end_time'           => 'required',
            'description'        => 'required',
            'photo'              => 'required',
            'publication_status' => 'required',
        ]);

        if ($request->start_date > $request->end_date){
            return redirect('/manan-administration2018/event/event-form')->with('aleart','Your Event Starting Time is greater than Event Ending Time!!!Select the correct time.');
        }

       $eventFile = $request->file('photo');
       $fileName  = $eventFile->getClientOriginalName();
       $directory = 'admin/event/';
       $url       = $directory.$fileName;
       Image::make($eventFile)->save($url);

        $event = new Event();

        $event->event_name         = $request->event_name;
        $event->event_category_id  = $request->event_category_id;
        $event->location           = $request->location;
        $event->start_date         = $request->start_date;
        $event->start_time         = $request->start_time;
        $event->end_date           = $request->end_date;
        $event->end_time           = $request->end_time;
        $event->description        = $request->description;
        $event->photo              = $url;
        $event->publication_status = $request->publication_status;

        $event->save();

        return redirect('/manan-administration2018/event/event-form')->with('message','Create event successfully');


    }

    public function manageEvent () {
        //manage event return to event manage table

        //return DB::table('events')->get();
        $events = DB::table('events')->join('event_categories','events.event_category_id','=','event_categories.id')->select('events.*','event_categories.event_type')->orderBy('events.id','desc')->paginate(5);
        return view('admin.event.event-manage',[
            'events' => $events
        ]);
    }

    public function unpublishedEvent ($id ) {
        //ubpublishes event info

        DB::table('events')->where('id', $id)->update(['publication_status' => 0]);

        return redirect('/manan-administration2018/event/manage-event')->with('message', 'Unpublished event info successfully');
    }

    public function publishedEvent ($id ) {
        //publishes event info

        DB::table('events')->where('id', $id)->update(['publication_status' => 1]);

        return redirect('/manan-administration2018/event/manage-event')->with('message', 'Published event info successfully');
    }

    public function editableEventForm ($id ) {
        //return to editable event form to update event info

        $eventById         =  Event::find($id);
        $eventCategoryById = EventCategory::find($eventById->event_category_id);

        return view('admin.event.editable-event-form', [
            'eventById'         => $eventById,
            'eventCategoryById' => $eventCategoryById
        ]);

    }

    public function updateEvent (Request $request ) {
        //update event info
        //return $request->all();

        $this->validate($request, [
            'event_name'         => 'required',
            'event_category_id'  => 'required',
            'location'           => 'required',
            'start_date'         => 'required',
            'start_time'         => 'required',
            'end_date'           => 'required',
            'end_time'           => 'required',
            'description'        => 'required',
            'photo'              => 'required',
            'publication_status' => 'required',
        ]);

        if ($request->start_date > $request->end_date){
            return redirect('/manan-administration2018/event/editable-event-form/'.$request->event_id )->with('aleart','Your Event Starting Time is greater than Event Ending Time!!!Select the correct time.');
        }

        $eventFile = $request->file('photo');
        $fileName  = $eventFile->getClientOriginalName();
        $directory = 'admin/event/';
        $url       = $directory.$fileName;
        Image::make($eventFile)->save($url);

        $eventById = Event::find($request->event_id);

        $eventById->event_name          = $request->event_name;
        $eventById->location            = $request->location;
        $eventById->start_date          = $request->start_date;
        $eventById->start_time          = $request->start_time;
        $eventById->end_date            = $request->end_date;
        $eventById->end_time            = $request->end_time;
        $eventById->description         = $request->description;
        $eventById->photo               = $url;
        $eventById->publication_status  = $request->publication_status;

        $eventById -> save();

        return redirect('/manan-administration2018/event/manage-event')->with('message','Update event info successfully');
    }

    public function deleteEvent ($id) {
        //delete event info

        $eventById = Event::find($id);
        $eventById->delete();

       return redirect('/manan-administration2018/event/manage-event')->with('message','Event info delete successfully');
    }


    public function viewEvent ($id) {
        $eventById = Event::find($id);
        return view('admin.event.view-event',[
            'eventById' => $eventById
        ]);
    }
}
