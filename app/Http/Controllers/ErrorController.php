<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ErrorController extends Controller
{
    public function error500($aleart ) {

        return view('admin.error.error500',[
            'aleart' => $aleart
        ]);
    }
}
