<?php

namespace App\Http\Controllers;

use App\Blog;
use App\BlogCategory;
use Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{
    public function blogPostForm () {
        //return to blog post form

      $publishedBlogCategories =  DB::table('blog_categories')->where('publication_status',1)->get();
        return view('admin.blog.blog-post-form',[
            'publishedBlogCategories' => $publishedBlogCategories
        ]);
    }

    /**
     * @param Request $request
     */
    public function blogPost (Request $request ) {
        //save blog post

        //return $request->all();
        $this->validate($request, [
            'blog_title'         => 'required',
            'blog_category_id'   => 'required',
            'writer'             => 'required',
            'submitted_date'     => 'required',
            'image'              => 'required',
            'description'        => 'required',
            'publication_status' => 'required',
        ]);

        $image = $request->file('image');
        $imageName = $image->getClientOriginalName();
        $directory = 'admin/blog-images/';
        $imageUrl = $directory . $imageName;
        Image::make($image)->save($imageUrl);

        $blog = new Blog();
        $blog->blog_title         = $request->blog_title;
        $blog->blog_category_id   = $request->blog_category_id;
        $blog->writer             = $request->writer;
        $blog->submitted_date     = $request->submitted_date;
        $blog->image              = $imageUrl;
        $blog->description        = $request->description;
        $blog->publication_status = $request->publication_status;

        $blog->save();

        return redirect('/manan-administration2018/blog/blog-post-form')->with('message','Blog post successfully');
    }

    public function manageBlog () {
        //manage blog return to blog manage table

        $blogs = DB::table('blogs')->join('blog_categories','blogs.blog_category_id','=','blog_categories.id')->select('blogs.*','blog_categories.blog_category')->orderBy('id','desc')->paginate(5);

        return view('admin.blog.blog-manage',[
            'blogs' => $blogs
        ]);
    }

    public function unpublishedBlogInfo ($id ) {
        //unpublished blog info

        DB::table('blogs')->where('id', $id)->update(['publication_status' => 0]);

        return redirect('/manan-administration2018/blog/manage-blog')->with('message', 'Unpublished blog info successfully');
    }

    public function publishedBlogInfo ($id ) {
        //published blog info

        DB::table('blogs')->where('id', $id)->update(['publication_status' => 1]);

        return redirect('/manan-administration2018/blog/manage-blog')->with('message', 'Published blog info successfully');
    }

    public function editableBlogForm ($id ) {
        //return to editable blog from

        $blogById         =  Blog::find($id);
        $blogCategoryById = BlogCategory::find($blogById->blog_category_id);

        return view('admin.blog.editable-blog-form', [
            'blogById'         => $blogById,
            'blogCategoryById' => $blogCategoryById
        ]);
    }

    public function updateBlogInfo (Request $request ) {
        //update blog info

        $this->validate($request, [
            'blog_title'         => 'required',
            'blog_category_id'   => 'required',
            'writer'             => 'required',
            'submitted_date'     => 'required',
            'description'        => 'required',
            'publication_status' => 'required',
        ]);

       $blogInfos = DB::table('blogs')->get();
       foreach ($blogInfos as $blogInfo ) {
           if ($request->image == null ) {

               $blog = Blog::find($request->blog_id);

               $blog->blog_title         = $request->blog_title;
               $blog->blog_category_id   = $request->blog_category_id;
               $blog->writer             = $request->writer;
               $blog->submitted_date     = $request->submitted_date;
               $blog->description        = $request->description;
               $blog->publication_status = $request->publication_status;

               $blog->save();

               return redirect('/manan-administration2018/blog/manage-blog')->with('message','Blog update successfully');
           }
       }

        $image = $request->file('image');
        $imageName = $image->getClientOriginalName();
        $directory = 'admin/blog-images/';
        $imageUrl = $directory . $imageName;
        Image::make($image)->save($imageUrl);

        $blog = Blog::find($request->blog_id);

        $blog->blog_title         = $request->blog_title;
        $blog->blog_category_id   = $request->blog_category_id;
        $blog->writer             = $request->writer;
        $blog->submitted_date     = $request->submitted_date;
        $blog->image              = $imageUrl;
        $blog->description        = $request->description;
        $blog->publication_status = $request->publication_status;

        $blog->save();

        return redirect('/manan-administration2018/blog/manage-blog')->with('message','Blog update successfully');
    }

    public function deleteBlogInfo ($id ) {
        //delete blog info

        Blog::find($id )->delete();
        return redirect('/manan-administration2018/blog/manage-blog')->with('message','Delete blog info successfully');
    }

    public function viewBlogInfo ($id ) {
        //view blog info

       $blogById = Blog::find($id );
       $blogCategoryById = BlogCategory::find($blogById->blog_category_id );
        return view('admin.blog.view-blog',[
            'blogById'          => $blogById,
            'blogCategoryById' => $blogCategoryById
        ]);
    }
}
