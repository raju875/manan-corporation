<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ShareController extends Controller
{
    public function shareFacebook ($id ){
        //return $id;
        $blogById = Blog::find($id);
        return view('front.share.facebook-share',[
            'blogById' => $blogById
        ]);
    }
}
